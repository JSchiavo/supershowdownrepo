﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof (SimonSays))]
[CanEditMultipleObjects]
public class SimonSaysEditor : Editor {

	SerializedProperty simon;
	SerializedProperty spring;
	SerializedProperty damper;
	SerializedProperty pin;
	SerializedProperty multi;

	bool showRotation;

	void OnEnable() {
		simon = serializedObject.FindProperty ("Simon");
		spring = serializedObject.FindProperty ("spring");
		damper = serializedObject.FindProperty ("damper");
		pin = serializedObject.FindProperty ("pin");
		multi = serializedObject.FindProperty ("muscleMultiplier");
	}

	public override void OnInspectorGUI ()
	{
		serializedObject.Update ();
		SimonSays SS = (SimonSays) target;
		if (GUILayout.Button ("Build Simon"))
			SS.BuildSimon ();
		simon.objectReferenceValue = (GameObject)EditorGUILayout.ObjectField ("Simon", simon.objectReferenceValue, typeof(GameObject), true);

		if (Application.isPlaying || Application.isEditor) {

			SS.show [0] = EditorGUILayout.Foldout (SS.show [0], "Root");
			if (SS.show [0]) {
				spring.GetArrayElementAtIndex(0).floatValue = EditorGUILayout.Slider ("Position Spring", SS.spring [0], 0f, 1000f);
				damper.GetArrayElementAtIndex(0).floatValue = EditorGUILayout.Slider ("Position Damper", SS.damper [0], 0f, 100f);
				pin.GetArrayElementAtIndex(0).floatValue = EditorGUILayout.Slider ("Pin Strength", SS.pin [0], 0f, 1f);
				multi.GetArrayElementAtIndex (0).floatValue = EditorGUILayout.Slider ("Muscle Multiplier", SS.muscleMultiplier [0], 1f, 10f);
				SS.UpdateMuscleGroup (MuscleGroup.All, SS.spring [0], SS.damper [0], SS.pin [0], SS.muscleMultiplier[0]);
			}
			SS.show [1] = EditorGUILayout.Foldout (SS.show [1], "Spine");
			if (SS.show [1]) {
				spring.GetArrayElementAtIndex(1).floatValue = EditorGUILayout.Slider ("Position Spring", SS.spring [1], 0f, 1000f);
				damper.GetArrayElementAtIndex(1).floatValue = EditorGUILayout.Slider ("Position Damper", SS.damper [1], 0f, 100f);
				pin.GetArrayElementAtIndex(1).floatValue = EditorGUILayout.Slider ("Pin Strength", SS.pin [1], 0f, 1f);
				multi.GetArrayElementAtIndex (1).floatValue = EditorGUILayout.Slider ("Muscle Multiplier", SS.muscleMultiplier [1], 1f, 10f);
				SS.UpdateMuscleGroup (MuscleGroup.Root, SS.spring [1], SS.damper [1], SS.pin [1], SS.muscleMultiplier[1]);
			}
			SS.show [2] = EditorGUILayout.Foldout (SS.show [2], "Head");
			if (SS.show [2]) {
				spring.GetArrayElementAtIndex(2).floatValue = EditorGUILayout.Slider ("Position Spring", SS.spring [2], 0f, 1000f);
				damper.GetArrayElementAtIndex(2).floatValue = EditorGUILayout.Slider ("Position Damper", SS.damper [2], 0f, 100f);
				pin.GetArrayElementAtIndex(2).floatValue = EditorGUILayout.Slider ("Pin Strength", SS.pin [2], 0f, 1f);
				multi.GetArrayElementAtIndex (2).floatValue = EditorGUILayout.Slider ("Muscle Multiplier", SS.muscleMultiplier [2], 1f, 10f);
				SS.UpdateMuscleGroup (MuscleGroup.Spine, SS.spring [2], SS.damper [2], SS.pin [2], SS.muscleMultiplier[2]);
			}
			SS.show [3] = EditorGUILayout.Foldout (SS.show [3], "Arms");
			if (SS.show [3]) {
				spring.GetArrayElementAtIndex(3).floatValue = EditorGUILayout.Slider ("Position Spring", SS.spring [3], 0f, 1000f);
				damper.GetArrayElementAtIndex(3).floatValue = EditorGUILayout.Slider ("Position Damper", SS.damper [3], 0f, 100f);
				pin.GetArrayElementAtIndex(3).floatValue = EditorGUILayout.Slider ("Pin Strength", SS.pin [3], 0f, 1f);
				multi.GetArrayElementAtIndex (3).floatValue = EditorGUILayout.Slider ("Muscle Multiplier", SS.muscleMultiplier [3], 1f, 10f);
				SS.UpdateMuscleGroup (MuscleGroup.Head, SS.spring [3], SS.damper [3], SS.pin [3], SS.muscleMultiplier[3]);
			}
			SS.show [4] = EditorGUILayout.Foldout (SS.show [4], "Legs");
			if (SS.show [4]) {
				spring.GetArrayElementAtIndex(4).floatValue = EditorGUILayout.Slider ("Position Spring", SS.spring [4], 0f, 1000f);
				damper.GetArrayElementAtIndex(4).floatValue = EditorGUILayout.Slider ("Position Damper", SS.damper [4], 0f, 100f);
				pin.GetArrayElementAtIndex(4).floatValue = EditorGUILayout.Slider ("Pin Strength", SS.pin [4], 0f, 1f);
				multi.GetArrayElementAtIndex (4).floatValue = EditorGUILayout.Slider ("Muscle Multiplier", SS.muscleMultiplier [4], 1f, 10f);
				SS.UpdateMuscleGroup (MuscleGroup.Arms, SS.spring [4], SS.damper [4], SS.pin [4], SS.muscleMultiplier[4]);
			}
		}

		if (GUILayout.Button ("Clean Ragdoll"))
			SS.Clean ();

		serializedObject.ApplyModifiedProperties ();
	}
}
