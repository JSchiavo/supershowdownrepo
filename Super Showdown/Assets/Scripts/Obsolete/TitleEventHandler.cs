﻿using UnityEngine;
using System.Collections;

public class TitleEventHandler : MonoBehaviour {

	public GameObject fx;

	public void InstantiateEffect() {
		GameObject o = (GameObject) GameObject.Instantiate (fx, gameObject.transform.position - gameObject.transform.up * 5f, Quaternion.Euler(new Vector3(75f, 0, 0)));
		o.transform.localScale = new Vector3 (20, 20, 20);
	}
}
