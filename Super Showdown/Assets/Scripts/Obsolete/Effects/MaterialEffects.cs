﻿using UnityEngine;
using System.Collections;

public class MaterialEffects : MonoBehaviour {

	public static void Invisible(Material mat) {
		Color c = mat.color;
		c.a = 0.0f;
	}

	public static IEnumerator VanishIn(Material mat) {
		for (float i = 0.0f; i < 1.0f; i += 0.1f) {
			Color c = mat.color;
			c.a = i;
			yield return null;
		}
	}
}
