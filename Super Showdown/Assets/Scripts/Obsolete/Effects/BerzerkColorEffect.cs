﻿using UnityEngine;
using System.Collections;

public class BerzerkColorEffect : MonoBehaviour {
	private Material mat;
	private bool end;

	private bool m_active;
	public bool active {
		get { return m_active; }
		set {
			if (value == false)
				mat.color = Color.white;
			m_active = value;
		}
	}

	// Use this for initialization
	void Start () {
		mat = gameObject.GetComponent<Renderer> ().material;
		end = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (m_active) {
			if (!end) {
				mat.color = Color.Lerp (mat.color, Color.red, Time.deltaTime * 2f);
				if (mat.color.g <= .1f)
					end = true;
			} else {
				mat.color = Color.Lerp (mat.color, Color.white, Time.deltaTime * 2f);
				if (mat.color.g >= .9f)
					end = false;
			}
		}

	}
}
