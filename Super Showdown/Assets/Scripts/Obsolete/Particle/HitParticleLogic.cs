﻿using UnityEngine;
using System.Collections;

public class HitParticleLogic : MonoBehaviour {

	private ParticleSystem ps;
	private AudioSource aud;

	// Use this for initialization
	void Start () {
		ps = gameObject.GetComponent<ParticleSystem> ();
		aud = gameObject.GetComponent<AudioSource> ();
		aud.Play ();
	}

	// Update is called once per frame
	void Update () {
		if (!ps.IsAlive())
			Destroy (gameObject);
	}
}
