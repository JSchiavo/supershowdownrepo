﻿using UnityEngine;
using System.Collections;

public class ChargeParticleLogic : MonoBehaviour {
	private ParticleSystem pS;
	private ParticleSystem.Particle[] particleList;

	// Use this for initialization
	void Start () {
		pS = gameObject.GetComponent<ParticleSystem> ();
	}
	
	// Update is called once per frame
	void Update () {
		/*Vector3 endPoint = gameObject.transform.up * -2f;

		particleList = new ParticleSystem.Particle[pS.particleCount];
		pS.GetParticles (particleList);

		for (int i = 0; i < particleList.Length; i++) {
			particleList [i].position = Vector3.Slerp (particleList[i].position, endPoint, Time.deltaTime);
			particleList [i].rotation = Mathf.Lerp (particleList [i].rotation, -90, Time.deltaTime * 5f);
		}
		//Debug.Log (particleList [0].rotation);

		pS.SetParticles (particleList, particleList.Length);*/

		if (!pS.IsAlive (false))
			Destroy (pS.gameObject);
	}

	public void Kill() {
		pS.loop = false;
	}
}
