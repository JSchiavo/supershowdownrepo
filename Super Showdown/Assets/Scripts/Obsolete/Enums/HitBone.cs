﻿using UnityEngine;
using System.Collections;

namespace States {

	public enum HitBone {
		LeftHand,
		RightHand,
		Chest,
		LeftLeg,
		RightLeg
	}
}
