﻿//using UnityEngine;
using System.Collections;

namespace States {
	public enum CameraState {
		NORMAL,
		METEOR1,
		METEOR2,
		SMASH,
		BLAST,
		LAUNCH,
		TARGET,
		FREE
	}
}
