﻿using System;

namespace States {
	public enum FightingState {
		BLOCK,
		NEUTRAL,
		LAUNCHED,
		FIGHTING,
		NOHIT
	}

	public enum HitType {
		WEAK,
		STRONG

	}
}

