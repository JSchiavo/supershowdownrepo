﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimIK : MonoBehaviour {

	public bool active = true;
	[Range(0f, 1f)]
	public float aimRate;
	private Animator anim;
	private Base b;
	private Quaternion spineSpace;


	// Use this for initialization
	void Start () {
		anim = gameObject.GetComponent<Animator> ();
		spineSpace = anim.GetBoneTransform (HumanBodyBones.Spine).rotation;
		b = gameObject.GetComponentInParent<Base> ();
	}
	
	void LateUpdate() {
		if (active && b.m.cM != null) {
			Transform spine = anim.GetBoneTransform (HumanBodyBones.Spine);
			Quaternion spineRotation = spine.rotation;
			Vector3 camDir = b.m.cM.cameraTransform.forward;
			camDir.y = 0f;

			Quaternion targetRotation = Quaternion.LookRotation (camDir, Vector3.up) * spineSpace;
			spine.rotation = Quaternion.Lerp (spine.rotation, targetRotation, aimRate);
		}

	}
}
