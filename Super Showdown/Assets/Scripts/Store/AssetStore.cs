﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetStore : MonoBehaviour {

	public GameObject[] store;

	public GameObject InstantiateObjectAtIndex(int index, Vector3 position, Quaternion rotation) {
		return (GameObject) GameObject.Instantiate (store [index], position, rotation);
	}

}
