﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerlinScale : MonoBehaviour {

	Vector3 startingScale;
	[Range(0.01f, 10f)]
	public float height;
	public float speed;

	void Start() {
		startingScale = transform.localScale;
	}

	// Update is called once per frame
	void Update () {
		float offset = Mathf.PerlinNoise (Time.time * speed, Time.time * speed) * height;

		transform.localScale = new Vector3 (startingScale.x + offset, startingScale.y + offset, startingScale.z + offset);
	}
}
