﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationComponent : BaseElement {

	public Animator anim;
	private bool[] layerBlendRoutinesRunning;
	private bool[] attackRoutinesRunning;
	public override void Initiate ()
	{
		base.Initiate ();
		anim = gameObject.GetComponent<Animator> ();
		layerBlendRoutinesRunning = new bool[anim.layerCount];
		attackRoutinesRunning = new bool[5];
	}

	public override void SubscribeMethods ()
	{
		base.SubscribeMethods ();
		e.OnGroundEnter += OnGroundEnter;
		e.OnGroundExit += OnGroundExit;
		e.RepinSimon += RepinSimon;
		e.UnpinSimon += UnpinSimon;
		e.BeginWeakCombo += BeginWeakCombo;
		e.OnWeakAttackEnter += OnWeakAttackEnter;
		e.OnWeakAttackExit += OnWeakAttackExit;
		e.OnStrongAttackEnter += OnStrongAttackEnter;
		e.OnStrongAttackExit += OnStrongAttackExit;
		e.BeginStrongAttack += BeginStrongAttack;
		e.EndStrongAttack += EndStrongAttack;
		e.EndWeakCombo += EndWeakCombo;
		e.OnProjectile += OnProjectile;
		e.OnBlock += OnBlock;
		e.OnBlockEnter += OnBlockEnter;
		e.OnBlockExit += OnBlockExit;
		e.OnBlast += OnBlast;
		e.OnBlastExit += OnBlastExit;
		e.OnBlastEnter += OnBlastEnter;
	}

	void Update() {
		if (initiate) {
			UpdateAnimatorValues ();
		}
	}

	void UpdateAnimatorValues() {
		anim.SetFloat ("Vert", playerModel.relativeVelocity.z / playerModel.maxSpeed);
		anim.SetFloat ("Hor", playerModel.relativeVelocity.x / playerModel.maxSpeed);
		anim.SetFloat ("Grav", playerModel.velocityY / playerModel.fallThreshold);
		anim.SetBool ("Grounded", playerModel.isGrounded);
		anim.SetBool ("Pinned", simonModel.isPinned);
		anim.SetBool ("Up", i.IsLeftStickUp ());
		anim.SetBool ("Down", i.IsLeftStickDown ());
		anim.SetBool ("Right", i.IsLeftStickRight ());
		anim.SetBool ("Left", i.IsLeftStickLeft ());
		anim.SetFloat ("Impact_Distance", simonModel.impactDistance);
	}

	private void BlendLayerFromTo (int layer, float startWeight, float targetWeight, float blendRate, float delay) {
		StartCoroutine (Blend (layer, startWeight, targetWeight, blendRate, delay));
	}

	private void BlendIntoAndOutOfLayer (int layer, float startWeight, float targetWeight, float endWeight, float blendRate, float delay) {
		StartCoroutine (BlendInOut(layer, startWeight, targetWeight, endWeight, blendRate, delay));
	}

	private IEnumerator Blend (int layer, float startWeight, float targetWeight, float blendRate, float delay) {
		if (layerBlendRoutinesRunning[layer])
			layerBlendRoutinesRunning [layer] = false;

		yield return null;

		layerBlendRoutinesRunning [layer] = true;


		anim.SetLayerWeight (layer, startWeight);
		float time = Time.time + delay;
		while (Time.time < time && layerBlendRoutinesRunning[layer]) {
			yield return null;
		}

		float delta = targetWeight - startWeight;
		if (delta < 0f) delta *= -1;
		delta *= blendRate;



		while (anim.GetLayerWeight (layer) != targetWeight && layerBlendRoutinesRunning[layer]) {
			anim.SetLayerWeight(layer, Mathf.MoveTowards(anim.GetLayerWeight(layer), targetWeight, delta));
			yield return null;
		}

		anim.SetLayerWeight (layer, targetWeight);
	}

	private IEnumerator BlendInOut (int layer, float startWeight, float targetWeight, float endWeight, float blendRate, float delay) {
		if (layerBlendRoutinesRunning [layer])
			layerBlendRoutinesRunning [layer] = false;

		yield return null;

		layerBlendRoutinesRunning [layer] = true;

		anim.SetLayerWeight (layer, startWeight);
		float delta = targetWeight - startWeight;
		if (delta < 0f) delta *= -1;
		delta *= blendRate;

		while (anim.GetLayerWeight (layer) != targetWeight && layerBlendRoutinesRunning[layer]) {
			anim.SetLayerWeight(layer, Mathf.MoveTowards(anim.GetLayerWeight(layer), targetWeight, delta));
			yield return null;
		}

		anim.SetLayerWeight (layer, targetWeight);

		float time = Time.time + delay;
		while (time > Time.time && layerBlendRoutinesRunning[layer]) {
			yield return null;
		}

		delta = endWeight - targetWeight;
		if (delta < 0f) delta *= -1;
		delta *= blendRate;

		while (anim.GetLayerWeight (layer) != endWeight && layerBlendRoutinesRunning[layer]) {
			anim.SetLayerWeight (layer, Mathf.MoveTowards (anim.GetLayerWeight (layer), endWeight, delta));
			yield return null;
		}

		anim.SetLayerWeight (layer, endWeight);
	}

	private IEnumerator WeakComboHandler() {
		attackRoutinesRunning [0] = false;

		yield return null;
		fightingModel.comboStep += 1;

		if (fightingModel.comboStep > 2)
			fightingModel.comboStep = 0;
		if (fightingModel.weakAttackLayer[fightingModel.comboStep] == 0) {
			BlendIntoAndOutOfLayer (3, anim.GetLayerWeight (3), 0f, 1f, 0.1f, 0.9f);
		}

		anim.SetInteger ("Weak_Combo", fightingModel.comboStep);
		fightingModel.canCombo = true;

		e.WeakAttackSetup (this, null);
		attackRoutinesRunning [0] = true;

		bool b_enter = false;
		bool b_exit = false;

		while (attackRoutinesRunning [0]) {
			AnimatorStateInfo attackState = anim.GetCurrentAnimatorStateInfo (fightingModel.weakAttackLayer[fightingModel.comboStep]);
			if (attackState.IsTag (fightingModel.weakAttackTags [fightingModel.comboStep])) {
				
				fightingModel.isMobile = (fightingModel.weakAttackLayer [fightingModel.comboStep] != 0);

				if (fightingModel.canCombo) {
					if (attackState.normalizedTime > fightingModel.weakComboAllowedAfter [fightingModel.comboStep] &&
					       attackState.normalizedTime < fightingModel.weakComboAllowedBefore [fightingModel.comboStep]) {
						if (i.GetButtonXDown ()) {
							fightingModel.canCombo = false;
						}
					}
				}

				if (!b_enter) {
					if (attackState.normalizedTime >= fightingModel.weakAttackEnter [fightingModel.comboStep]) {
						b_enter = true;
						e.OnWeakAttackEnter (this, null);
					}
				}

				if (!b_exit) {
					if (attackState.normalizedTime >= fightingModel.weakAttackExit [fightingModel.comboStep]) {
						b_exit = true;
						e.OnWeakAttackExit (this, null);
					}
				}

				if (attackState.normalizedTime >= fightingModel.weakComboAllowedBefore[fightingModel.comboStep]) {
					if (!fightingModel.canCombo) {
						StartCoroutine (WeakComboHandler ());
					} else {
						attackRoutinesRunning [0] = false;
						e.EndWeakCombo (this, null);
					}
				} 
			}
			yield return null;
		}
		if (!b_exit)
			e.OnWeakAttackExit (this, null);
	}

	private IEnumerator StrongAttackHandler() {
		attackRoutinesRunning [1] = false;

		while (anim.IsInTransition (0))
			yield return null;

		yield return null;

		anim.SetInteger ("Strong_Direction", fightingModel.strongDirection);

		attackRoutinesRunning [1] = true;

		WaitForSeconds chargeTimer = new WaitForSeconds (0.1f);

		float multiplier = 1f;
		bool b_event = false;
		bool b_chance = false;
		bool b_enter = false;
		bool b_exit = false;
		float endChargeTime = Mathf.Infinity;
		fightingModel.isMobile = false;

		while (attackRoutinesRunning [1]) {

			AnimatorStateInfo attackState = anim.GetCurrentAnimatorStateInfo (0);
			if (attackState.IsTag (fightingModel.strongAttackTags [fightingModel.strongDirection])) {
				if (!b_chance) {
					if (attackState.normalizedTime < fightingModel.chargeChanceAt [fightingModel.strongDirection]) {
						while (i.GetButtonYHold () && Time.time < endChargeTime) {
							attackState = anim.GetCurrentAnimatorStateInfo (0);
							if (!b_chance) {
								endChargeTime = Time.time + fightingModel.maxChargeTime;
								b_chance = true;
							}
							if (attackState.normalizedTime > fightingModel.chargeChanceAt [fightingModel.strongDirection])
								anim.speed = 0f;
							multiplier += 0.25f;
							yield return chargeTimer;
						}
						fightingModel.chargeMultiplier = multiplier;
						anim.speed = 1f;
					}
				}

				if (!b_event) {
					if (attackState.normalizedTime > fightingModel.strongEventAt [fightingModel.strongDirection]) {
						b_event = true;
						if (e.OnStrongAttackEvent != null)
							e.OnStrongAttackEvent (this, null);
					}
				}

				if (!b_enter) {
					if (attackState.normalizedTime > fightingModel.StrongAttackEnter [fightingModel.strongDirection]) {
						b_enter = true;
						e.OnStrongAttackEnter (this, null);
					}
				}

				if (!b_exit) {
					if (attackState.normalizedTime > fightingModel.strongAttackExit [fightingModel.strongDirection]) {
						b_exit = true;
						e.OnStrongAttackExit (this, null);

					}
				}

				if (anim.IsInTransition (0)) {
					e.EndStrongAttack (this, null);
					attackRoutinesRunning [1] = false;
				}
			}
			yield return null;
		}

		if (!b_exit) {
			e.OnStrongAttackExit (this, null);
		}
	}

	private IEnumerator ProjectileHandler() {
		attackRoutinesRunning [2] = false;

		yield return null;

		attackRoutinesRunning [2] = true;

		fightingModel.isShooting = true;

		bool b_shoot = false;
		bool b_recharge = false;

		while (attackRoutinesRunning[2]) {
			AnimatorStateInfo projectileState = anim.GetCurrentAnimatorStateInfo (3);
			if (projectileState.IsTag(fightingModel.projectileTag)) {

				if (!b_shoot) {
					if (projectileState.normalizedTime > fightingModel.projectileShootAt) {
						b_shoot = true;
						Base enemy = game.GetEnemy (b);
						Vector3 forward = camModel.cameraTransform.forward;
						forward.y = forward.y < 0f ? 0f : forward.y;
						GameObject projectile = game.assetStore.InstantiateObjectWithKey ("EnergyBlast", anim.GetBoneTransform (HumanBodyBones.RightHand).position, Quaternion.LookRotation(forward.normalized));
						projectile.GetComponent<ProjectileLogic>().SetTarget(enemy);
					}
				}

				if (!b_recharge) {
					if (projectileState.normalizedTime > fightingModel.projectileRechargeAt) {
						b_recharge = true;
						fightingModel.isShooting = false;
						attackRoutinesRunning [2] = false;
					}
				}
			}
			yield return null;
		}
	}

	private IEnumerator BlockHandler() {
		attackRoutinesRunning [3] = false;

		yield return null;

		attackRoutinesRunning [3] = true;

		bool b_enter = false;
		bool b_chance = false;
		bool b_exit = false;

		while (attackRoutinesRunning [3]) {
			AnimatorStateInfo blockState = anim.GetCurrentAnimatorStateInfo (3);
			if (blockState.IsTag (fightingModel.blockTag)) {

				if (!b_enter) {
					if (blockState.normalizedTime > fightingModel.blockEnter) {
						e.OnBlockEnter (this, null);
						b_enter = true;
					}
				}

				if (!b_chance) {
					if (blockState.normalizedTime > fightingModel.blockChance) {
						while (i.GetRightTriggerHold ()) {
							anim.speed = 0.0f;
							yield return null;
						}
						b_chance = true;
						anim.speed = 1.0f;
					}
				}

				if (!b_exit) {
					if (blockState.normalizedTime > fightingModel.blockExit) {
						e.OnBlockExit (this, null);
						b_exit = true;
						attackRoutinesRunning [3] = false;
					}
				}
			}

			yield return null;
		}
		if (!b_exit) {
			e.OnBlockExit (this, null);
		}

	}

	private IEnumerator BlastHandler() {
		attackRoutinesRunning [4] = false;

		yield return null;

		attackRoutinesRunning [4] = true;

		bool b_enter = false;
		bool b_exit = false;

		while (attackRoutinesRunning [4]) {
			AnimatorStateInfo attackState = anim.GetCurrentAnimatorStateInfo (0);
			if (attackState.IsTag (fightingModel.blastTag)) {
				if (i.GetLeftTriggerHold () || b_enter) {
					float time = attackState.normalizedTime;

					if (time < 0.45f)
						anim.speed = 0.2f;
					else if (time < fightingModel.blastEnter)
						anim.speed = 1f;
					else if (time < 0.79f)
						anim.speed = 0.04f;
					else
						anim.speed = 1f;
				} else if (!b_enter) {
					anim.speed = 1f;
					attackRoutinesRunning [4] = false;
				}

				if (!b_enter) {
					if (attackState.normalizedTime > fightingModel.blastEnter) {
						e.OnBlastEnter (this, null);
						b_enter = true;
					}
				}

				if (!b_exit) {
					if (attackState.normalizedTime > fightingModel.blastExit) {
						e.OnBlastExit (this, null);
						b_exit = true;
					}
				}
			}
			yield return null;
		}
		if (!b_exit)
			e.OnBlastExit (this, null);
	}

	private CharacterModels playerModel {
		get {
			return b.m.pM;
		}
	}

	private CameraModel camModel {
		get {
			return b.m.cM;
		}
	}

	private SimonModel simonModel {
		get {
			return b.m.sM;
		}
	}

	private FightingModel fightingModel {
		get {
			return b.m.fM;
		}
	}

	public override int priority {
		get {
			return 2;
		}
	}

	//EventMethods
	public void OnStrongAttackEnter(object sender, object data) {
		playerModel.fightingState = FightingState.Strong;
		playerModel.hasHit = false;
		float permDamage = fightingModel.strongInitialPermanentDamage[fightingModel.strongDirection] * fightingModel.chargeMultiplier * playerModel.power;
		float knockback = fightingModel.strongInitialKnockback [fightingModel.strongDirection] * fightingModel.chargeMultiplier * playerModel.power;
		Vector3 direction = Vector3.zero;
		switch (fightingModel.strongDirection) {
		case 0:
			direction = playerModel.playerTransform.forward;
			direction.y = 0.2f;
			break;
		case 1:
			direction = playerModel.playerTransform.up;
			break;
		case 2:
			direction = playerModel.playerTransform.right;
			direction.y = 0f;
			break;
		case 3:
			direction = -playerModel.playerTransform.right;
			direction.y = 0f;
			break;
		}
		playerModel.currentAttack = new AttackData (FightingState.Strong, fightingModel.strongAttackMuscleGroup [fightingModel.strongDirection], direction.normalized, knockback, permDamage);
	}

	public void OnStrongAttackExit(object sender, object data) {
		anim.SetInteger ("Strong_Direction", -1);
		playerModel.fightingState = FightingState.Nuetral;
		playerModel.hasHit = false;
	}

	public void BeginStrongAttack (object sender, object data) {
		BlendLayerFromTo (3, anim.GetLayerWeight (3), 0f, 0.1f, 0f);
		StartCoroutine (StrongAttackHandler ());
	}

	public void EndStrongAttack(object sender, object data) {
		BlendLayerFromTo (3, 0f, 1f, 0.1f, 0f);
		fightingModel.strongDirection = -1;
		fightingModel.isMobile = true;
	}

	public void BeginWeakCombo(object sender, object data) {
		StartCoroutine (WeakComboHandler ());
	}

	public void EndWeakCombo(object sender, object data) {
		fightingModel.comboStep = -1;
		fightingModel.isMobile = true;
		anim.SetInteger ("Weak_Combo", -1);
	}

	public void OnWeakAttackEnter(object sender, object data) {
		playerModel.fightingState = FightingState.Weak;
		playerModel.hasHit = false;
		AttackData attack;
		float permDamage = fightingModel.weakPermanentDamage[fightingModel.comboStep] * playerModel.power;
		attack = new AttackData (FightingState.Weak, fightingModel.weakAttackMuscleGroup[fightingModel.comboStep], playerModel.playerTransform.forward, fightingModel.weakKnockback[fightingModel.comboStep], permDamage);
		playerModel.currentAttack = attack;
	}

	public void OnWeakAttackExit(object sender, object data) {
		playerModel.fightingState = FightingState.Nuetral;
		playerModel.hasHit = false;
	}

	public void OnGroundEnter(object sender, object data) {
		if (simonModel.isPinned) {
			BlendLayerFromTo (3, anim.GetLayerWeight (3), 1f, 0.1f, 0f);
		}
	}

	public void OnGroundExit(object sender, object data) {
		if (simonModel.isPinned) {
			BlendLayerFromTo (3, anim.GetLayerWeight (3), 0f, 0.1f, 0f);
		}
	}

	public void OnProjectile(object sender, object data) {
		anim.SetTrigger ("Projectile");
		StartCoroutine (ProjectileHandler());
		BlendLayerFromTo (3, anim.GetLayerWeight (3), 1f, 0.15f, 0f);
		game.audioManager.PlayAudio ("Shot", this, false);
	}

	public void OnBlock(object sender, object data) {
		StartCoroutine (BlockHandler());
		BlendLayerFromTo (3, anim.GetLayerWeight (3), 1f, 0.1f, 0f);
		anim.SetTrigger ("Block");
	}

	public void OnBlockEnter(object sender, object data) {
		playerModel.fightingState = FightingState.Block;
		fightingModel.isMobile = false;
	}

	public void OnBlockExit(object sender, object data) {
		playerModel.fightingState = FightingState.Nuetral;
		fightingModel.isMobile = true;
		anim.ResetTrigger ("Block");
	}

	private GameObject blast;
	private GameObject lightningSphere;
	private GameObject debris;
	private GameObject orb;

	public void OnBlast(object sender, object data) {
		gameObject.GetComponent<AudioSource> ().Play ();
		anim.SetTrigger ("Blast");
		fightingModel.isMobile = false;
		StartCoroutine (BlastHandler ());
		BlendLayerFromTo (3, anim.GetLayerWeight (3), 0f, 0.1f, 0f);
		debris = game.assetStore.InstantiateObjectWithKey ("ChargeDebris", playerModel.playerTransform.position, Quaternion.Euler (-90f, 0f, 0f), playerModel.playerTransform);
		lightningSphere = game.assetStore.InstantiateObjectWithKey ("ChargeSphere",
			(anim.GetBoneTransform (HumanBodyBones.RightHand).position + anim.GetBoneTransform (HumanBodyBones.LeftHand).position) / 2f,
			Quaternion.identity, anim.GetBoneTransform(HumanBodyBones.RightHand));
		orb = game.assetStore.InstantiateObjectWithKey ("Orb",
			(anim.GetBoneTransform (HumanBodyBones.RightHand).position + anim.GetBoneTransform (HumanBodyBones.LeftHand).position) / 2f,
			Quaternion.identity, anim.GetBoneTransform (HumanBodyBones.RightHand));
	}

	public void OnBlastEnter(object sender, object data) {
		if (lightningSphere != null)
			lightningSphere.GetComponent<DestroyOnCompletion> ().Kill ();
		blast = game.assetStore.InstantiateObjectWithKey ("Beam",
			(anim.GetBoneTransform (HumanBodyBones.RightHand).position + anim.GetBoneTransform(HumanBodyBones.LeftHand).position)/2f,
			Quaternion.LookRotation (playerModel.playerTransform.forward));
		if (orb != null)
			orb.GetComponent<PerlinScale> ().speed = 1f;
	}

	public void OnBlastExit(object sender, object data) {
		fightingModel.isMobile = true;
		anim.ResetTrigger ("Blast");
		anim.speed = 1.0f;
		BlendLayerFromTo (3, anim.GetLayerWeight (3), 1f, 0.1f, 0f);
		if (lightningSphere != null)
			lightningSphere.GetComponent<DestroyOnCompletion> ().Kill ();
		if (debris != null)
			debris.GetComponent<DestroyOnCompletion> ().Kill ();
		if (blast != null)
			Destroy (blast);
		if (orb != null)
			Destroy (orb);
	}

	public void RepinSimon(object sender, object data) {
		StopAllCoroutines ();
		Vector3 hipDir = simonModel.ragdollRootTransform.forward;
		if (hipDir.y >= 0)
			anim.SetTrigger ("GetUpBack");
		else
			anim.SetTrigger ("GetUpBelly");
		BlendIntoAndOutOfLayer(2, anim.GetLayerWeight(2), 1f, 0f, 0.5f, 0.9f);
		BlendLayerFromTo (3, 0f, 1f, 0.1f, 2f);
	}
		
	public void UnpinSimon(object sender, object data) {
		StopAllCoroutines ();
		for (int i = 0; i < anim.layerCount; i++) {
			BlendLayerFromTo (i, anim.GetLayerWeight (i), 0f, 0.1f, 0f);
		}
	}
}
