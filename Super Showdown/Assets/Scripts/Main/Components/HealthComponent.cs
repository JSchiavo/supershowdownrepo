﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthComponent : BaseElement {

	public Image healthBar;
	private MuscleGroupStats stats;

	void Update() {
		if (initiate) {
			Vector3 size = healthBar.rectTransform.localScale;
			size.x = (100f - stats.permanentDamage) / 100f;
			healthBar.rectTransform.localScale = size;
			healthBar.color = Color.Lerp(Color.green, Color.red, stats.temporaryDamage / 100f);
		}
	}

	public override void Initiate ()
	{
		base.Initiate ();
		healthBar = gameObject.GetComponent<Image> ();
		healthBar.color = Color.green;
		stats = simonModel.GetMuscleGroupStats (MuscleGroup.Spine);
	}

	public SimonModel simonModel {
		get {
			return b.m.sM;
		}
	}

	public override int priority {
		get {
			return 3;
		}
	}
}
