﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraComponent : BaseElement {

	Camera cam;
	float deltaX;
	float deltaY;
	Transform target;

	public override void Initiate ()
	{
		base.Initiate ();
		cam = gameObject.GetComponent<Camera> ();
		if (camModel == null) {
			b.m.cM = b.m.gameObject.AddComponent<CameraModel> ();
			camModel.Initiate ();
		}
		camModel.cameraTransform = cam.transform;
		target = playerModel.playerTransform;
	}

	public override void SubscribeMethods ()
	{
		base.SubscribeMethods ();

		e.UnpinSimon += UnpinSimon;
		e.RepinSimon += RepinSimon;
	}

	void Update() {
		deltaX += i.GetRightStickV () * camModel.sensitivity;
		deltaY += i.GetRightStickH () * camModel.sensitivity;
		deltaX = Mathf.Clamp (deltaX, camModel.minAngleX, camModel.maxAngleX);
	}

	// Update is called once per frame
	void LateUpdate () {
		if (initiate) {
			cam.transform.position = targetPosition;
			Vector3 look;
			if (simonModel.isPinned)
				look = target.position + Vector3.up;
			else
				look = target.position;
			cam.transform.LookAt (look);
		}
	}

	private Vector3 targetPosition {
		get {
			Vector3 dir = new Vector3 (0, 0, -camModel.distance);
			Quaternion rotation = Quaternion.Euler (deltaX, deltaY, 0f);
			return target.position + rotation * (dir);
		}
	}

	private CameraModel camModel {
		get {
			return b.m.cM;
		}
	}

	private CharacterModels playerModel {
		get {
			return b.m.pM;
		}
	}

	private SimonModel simonModel {
		get {
			return b.m.sM;
		}
	}

	public override int priority {
		get {
			return 3;
		}
	}

	//EventMethods

	public void UnpinSimon(object sender, object data) {
		target = simonModel.ragdollRootTransform;
	}

	public void RepinSimon(object sender, object data) {
		target = playerModel.playerTransform;
	}
}
