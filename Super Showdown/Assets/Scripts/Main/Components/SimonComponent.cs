﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimonComponent : BaseElement {

	private SimonSays simon;
	private bool[] targetBlendRoutinesRunning;
	public override void Initiate ()
	{
		base.Initiate ();
		simon = gameObject.GetComponent<SimonSays> ();
		simonModel.targetRootTransform = simon.root.gameObject.GetComponent<Muscle>().target;
		simonModel.ragdollRootTransform = simon.root;
		if (simon == null) {
			Debug.LogError ("'SimonSays' missing from gameObject.");
			initiate = false;
			return;
		}
		targetBlendRoutinesRunning = new bool[5];
		simon.muscleHandler.OnMuscleCollisionStay += OnMuscleCollisionStay;
		simon.muscleHandler.OnMuscleCollisionEnter += OnMuscleCollisionEnter;
		StartBlendThreads ();
	}

	public override void SubscribeMethods ()
	{
		base.SubscribeMethods ();
		e.UnpinSimon += UnpinSimon;
		e.RepinSimon += RepinSimon;
		e.OnAttackReceived += OnAttackReceived;
	}

	public override int priority {
		get {
			return 2;
		}
	}

	void Update() {
		if (pin) {
			if (Vector3.Distance (simonModel.targetRootTransform.position, simonModel.ragdollRootTransform.position) > simonModel.unpinDistanceThreshold) {
				e.UnpinSimon (this, null);
			}
		}
	}

	public bool pin {
		get {
			return simonModel.isPinned;
		}
		set {
			simonModel.isPinned = value;
			foreach (MuscleGroupStats stats in simonModel.muscleGroupStats) {
				stats.isPinned = value;
			}
		}
	}
		
	private void StartBlendThreads () {
		MuscleGroupStats rootStats = simonModel.GetMuscleGroupStats (MuscleGroup.Root);
		MuscleGroupStats spineStats = simonModel.GetMuscleGroupStats (MuscleGroup.Spine);
		MuscleGroupStats headStats = simonModel.GetMuscleGroupStats (MuscleGroup.Head);
		MuscleGroupStats armStats = simonModel.GetMuscleGroupStats (MuscleGroup.Arms);
		MuscleGroupStats legStats = simonModel.GetMuscleGroupStats (MuscleGroup.Legs);

		StartCoroutine (Blending (0, rootStats));
		StartCoroutine (Blending (1, spineStats));
		StartCoroutine (Blending (2, headStats));
		StartCoroutine (Blending (3, armStats));
		StartCoroutine (Blending (4, legStats));
	}

	private IEnumerator Blending (int index, MuscleGroupStats stats) {
		targetBlendRoutinesRunning [index] = true;
		WaitForSeconds time = new WaitForSeconds (0.15f);

		while (targetBlendRoutinesRunning[index]) {

			stats.AddTemporaryDamage (-simonModel.tempDamageRecovery);

			bool hasChanged = false;
			if (!Mathf.Approximately (simon.spring [index], stats.positionSpringTarget)) {
				float springDelta = 1000f * simonModel.muscleBlend;
				simon.spring [index] = Mathf.Clamp(Mathf.MoveTowards (simon.spring [index], stats.positionSpringTarget, springDelta), 0f, 1000f);
				hasChanged = true;
			}
			if (!Mathf.Approximately (simon.damper [index], stats.positionDamperTarget)) {
				float springDelta = 100f * simonModel.muscleBlend;
				simon.damper [index] = Mathf.Clamp(Mathf.MoveTowards (simon.damper [index], stats.positionDamperTarget, springDelta), 0f, 100f);
				hasChanged = true;
			}
			//float pinTarget = stats.isPinned ? stats.health : 0f;
			if (!Mathf.Approximately(simon.pin[index], stats.pinStrengthTarget * stats.health)) {
				float pinDelta = 1f * simonModel.muscleBlend;
				simon.pin [index] = Mathf.Clamp(Mathf.MoveTowards (simon.pin [index], stats.pinStrengthTarget * stats.health, pinDelta), 0f, 1f);
				hasChanged = true;
			}
			float multiTarget = stats.isPinned ? stats.health * stats.muscleMultiplierTarget : stats.muscleMultiplierTarget;

			if (!Mathf.Approximately (simon.muscleMultiplier [index], multiTarget)) {
				float multiDelta = 10f * simonModel.muscleBlend;
				simon.muscleMultiplier [index] = Mathf.Clamp(Mathf.MoveTowards (simon.muscleMultiplier [index], multiTarget, multiDelta), 1f, 10f);
				hasChanged = true;
			}
			if (hasChanged)
				simon.SetWeights (stats.group);
			
			yield return time;
		}
	}

	private void UnpinImmediately() {
		if (pin)
			pin = false;
		for (int i = 0; i < 5; i++) {
			simon.spring [i] = 0f;
			simon.damper [i] = 0f;
			simon.pin [i] = 0f;
			simon.muscleMultiplier [i] = 1f;
		}
		simon.SetWeights (MuscleGroup.All);
	}

	private float CalculateMaxDelta(float current, float target, float blendRate) {
		float delta = (target - current) * simonModel.muscleBlend;
		if (delta < 0f)
			delta *= -1;
		return delta;
	}

	private float[] MuscleGroupDeltaValues(int index, float positionSpringTarget, float positionDamperTarget, float pinStrengthTarget, float muscleMultiTarget, float blendRate) {
		float[] deltas = new float[4];
		deltas[0] = CalculateMaxDelta (simon.spring [index], positionSpringTarget, blendRate);
		deltas[1] = CalculateMaxDelta (simon.damper [index], positionDamperTarget, blendRate);
		deltas[2] = CalculateMaxDelta (simon.pin [index], pinStrengthTarget, blendRate);
		deltas [3] = CalculateMaxDelta (simon.muscleMultiplier [index], muscleMultiTarget, blendRate);
		return deltas;
	}

	private IEnumerator CheckForImpact() {
		while (!pin) {
			Collider[] surroundings;
			float radius = simonModel.impactRadius;
			float impact = simonModel.impactRadius;
			surroundings = Physics.OverlapSphere (simonModel.ragdollRootTransform.position, radius, simonModel.collisionMask, QueryTriggerInteraction.Ignore);

			foreach (Collider col in surroundings){
				Vector3 closestPoint = col.ClosestPointOnBounds (simonModel.ragdollRootTransform.position);
				float distance = Vector3.Distance (closestPoint, simonModel.ragdollRootTransform.position);
				if (distance < impact)
					impact = distance;
			}
			simonModel.impactDistance = impact/radius;
			yield return null;
		}
	}

	private IEnumerator BeginRepinRecovery() {
		Rigidbody hip = simonModel.ragdollRootTransform.gameObject.GetComponent<Rigidbody> ();
		float recoveryTime = simonModel.recoveryTimer;

		while (recoveryTime > 0f) {
			if (playerModel.isGrounded) {
				float speed = hip.velocity.magnitude;
				if (speed < simonModel.recoveryVelocity) {
					recoveryTime -= Time.deltaTime;
				} else
					recoveryTime = simonModel.recoveryTimer;
			}
			yield return null;
		}

		e.RepinSimon (this, null);
	}

	private SimonModel simonModel {
		get {
			return b.m.sM;
		}
	}

	private CharacterModels playerModel {
		get {
			return b.m.pM;
		}
	}

	private void CalculateTempDamage(Muscle muscle, float damage) {
		if (muscle.group == MuscleGroup.Root) {
			MuscleGroupStats root = simonModel.GetMuscleGroupStats (MuscleGroup.Root);
			MuscleGroupStats spine = simonModel.GetMuscleGroupStats (MuscleGroup.Spine);
			MuscleGroupStats head = simonModel.GetMuscleGroupStats (MuscleGroup.Head);
			MuscleGroupStats arms = simonModel.GetMuscleGroupStats (MuscleGroup.Arms);
			MuscleGroupStats legs = simonModel.GetMuscleGroupStats (MuscleGroup.Legs);

			root.AddTemporaryDamage (damage);
			spine.AddTemporaryDamage (damage);
			head.AddTemporaryDamage (damage * 1.5f);
			arms.AddTemporaryDamage (damage * 1.2f);
			legs.AddTemporaryDamage (damage * 1.2f);
			return;
		}
		if (muscle.group == MuscleGroup.Spine) {
			MuscleGroupStats spine = simonModel.GetMuscleGroupStats (MuscleGroup.Spine);
			MuscleGroupStats head = simonModel.GetMuscleGroupStats (MuscleGroup.Head);
			MuscleGroupStats arms = simonModel.GetMuscleGroupStats (MuscleGroup.Arms);

			spine.AddTemporaryDamage(damage);
			head.AddTemporaryDamage(damage * 1.2f);
			arms.AddTemporaryDamage(damage * 1.2f);
			return;
		}
		if (muscle.group == MuscleGroup.Head) {
			MuscleGroupStats spine = simonModel.GetMuscleGroupStats (MuscleGroup.Spine);
			MuscleGroupStats head = simonModel.GetMuscleGroupStats (MuscleGroup.Head);
			MuscleGroupStats arms = simonModel.GetMuscleGroupStats (MuscleGroup.Arms);

			head.AddTemporaryDamage (damage);
			spine.AddTemporaryDamage (damage * 0.8f);
			arms.AddTemporaryDamage (damage * 0.8f);
			return;
		}
		if (muscle.group == MuscleGroup.Arms) {
			MuscleGroupStats arms = simonModel.GetMuscleGroupStats (MuscleGroup.Arms);

			arms.AddTemporaryDamage (damage);
			return;
		}
		if (muscle.group == MuscleGroup.Legs) {
			MuscleGroupStats legs = simonModel.GetMuscleGroupStats (MuscleGroup.Legs);
			MuscleGroupStats root = simonModel.GetMuscleGroupStats (MuscleGroup.Root);

			legs.AddTemporaryDamage (damage);
			root.AddTemporaryDamage (damage * 0.8f);
		}

	}

	//Event Functions

	public void UnpinSimon(object sender, object data) {
		UnpinImmediately ();
		StartCoroutine (CheckForImpact ());
		StartCoroutine (BeginRepinRecovery());
	}

	public void RepinSimon(object sender, object data) {
		pin = true;
	}

	public void OnAttackReceived(Base enemy, AttackData data, Collision collision) {
		Muscle muscle = collision.gameObject.GetComponent<Muscle> ();
		float magnitude = collision.relativeVelocity.magnitude;
		MuscleGroupStats stats = simonModel.GetMuscleGroupStats (muscle.group);
		stats.AddPermanentDamage (data.permanentDamage);
		CalculateTempDamage(muscle, Mathf.Clamp(data.permanentDamage * magnitude * 2f, 5f, 25f));
		if (data.type == FightingState.Weak)
			simon.AddForceToMuscleGroup (muscle.group, data.direction * data.knockback, ForceMode.VelocityChange);
		else if (data.type == FightingState.Strong) {
			if (pin)
				e.UnpinSimon (this, null);
			simon.AddForceToMuscleGroup (MuscleGroup.Root, data.direction * data.knockback, ForceMode.VelocityChange);
		}
	}

	public void OnMuscleCollisionEnter (Muscle sender, Collision collision) {
		if (collision.gameObject.layer == 9) {
			if (playerModel.currentAttack != null && playerModel.currentAttack.groupSource == sender.group) {
				if (!playerModel.hasHit && (playerModel.fightingState == FightingState.Strong || playerModel.fightingState == FightingState.Weak)) {
					Base enemy = collision.gameObject.GetComponentInParent<Base> ();
					enemy.e.OnAttackReceived (b, playerModel.currentAttack, collision);
					playerModel.hasHit = true;
				}
			}
		}
	}

	public void OnMuscleCollisionStay(Muscle sender, Collision collision) {
		if (collision.gameObject.layer != 9 && collision.gameObject.layer != 8 && !pin) {
			if (sender.group != MuscleGroup.Legs) {
				MuscleGroupStats stats = simonModel.GetMuscleGroupStats (sender.group);
				//stats.AddTemporaryDamage (1f);
			}
		}
	}
}
