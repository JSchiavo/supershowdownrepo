﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerComponent : BaseElement {

	//props
	private Vector3 lastVelocity;
	private float lastAcceleration;
	private float accelerationMag;
	private bool dblJump;
	private List<Coroutine> routines;

	//components
	public Rigidbody rigid;
	private AimIK aim;

	// Use this for initialization
	public override void Initiate () {
		base.Initiate ();
		rigid = gameObject.GetComponent<Rigidbody> () != null ? gameObject.GetComponent<Rigidbody> () : gameObject.AddComponent<Rigidbody> ();
		if (playerModel == null) {
			b.m.pM = b.m.gameObject.AddComponent<CharacterModels> ();
			playerModel.HeavyConfigurations ();
		}
		routines = new List<Coroutine> ();
		playerModel.playerTransform = transform;
		aim = gameObject.GetComponent<AimIK> ();
		foreach (SkinnedMeshRenderer mesh in gameObject.GetComponentsInChildren<SkinnedMeshRenderer> ()) {
			mesh.quality = SkinQuality.Bone2;
		}
	}

	public override void SubscribeMethods ()
	{
		base.SubscribeMethods ();
		e.OnGroundEnter += OnGroundEnter;
		e.OnGroundExit += OnGroundExit;
		e.UnpinSimon += UnpinSimon;
		e.RepinSimon += RepinSimon;
		e.OnWeakAttackEnter += OnWeakAttackEnter;
		e.OnStrong += OnStrong;
		e.OnAttackReceived += OnAttackReceived;
		e.WeakAttackSetup += WeakAttackSetup;
		e.EndWeakCombo += EndWeakCombo;
		e.BeginStrongAttack += BeginStrongAttack;
		e.EndStrongAttack += EndStrongAttack;
		e.OnStrongAttackEvent += OnStrongAttackEvent;
		e.OnBlast += OnBlast;
		e.OnBlastExit += OnBlastExit;
	}

	private void CancelRoutines() {
		foreach (Coroutine routine in routines) {
			if (routine != null) {
				StopCoroutine (routine);
			}
		}
		routines = new List<Coroutine> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (initiate) {
			CheckInput ();
			if (fightingModel.isMobile)
				rigid.rotation = Quaternion.Lerp (rigid.rotation, targetRotation, playerModel.turnSpeed);
			playerModel.isGrounded = isGrounded;
		}
	}

	void FixedUpdate() {
		if (initiate) {
			UpdateModel ();
			AddVelocity ();
		}
	}

	private void UpdateModel() {
		playerModel.relativeVelocity = relativeVelocity;
		playerModel.relativeAcceleration = relativeAccceleration;

	}

	private void CheckInput() {
		if (i != null) {
			if (fightingModel != null) {
				playerModel.v = fightingModel.isMobile ? i.GetLeftStickV () : 0f;
				playerModel.h = fightingModel.isMobile ? i.GetLeftStickH () : 0f;
			}

			if (simonModel.isPinned && fightingModel.isMobile) {
				if (playerModel.isGrounded) {
					if (i.GetButtonADown ()) {
						rigid.AddForce (Vector3.up * 10f, ForceMode.VelocityChange);
					} else if (i.GetButtonXDown ()) {
						if (fightingModel != null) {
							if (!fightingModel.isComboing && !fightingModel.isStrong) {
								e.BeginWeakCombo (this, null);
								return;
							}
						}
					} else if (i.GetButtonYDown () && !fightingModel.isStrong && !fightingModel.isComboing) {
						if (i.IsLeftStickUp ()) {
							fightingModel.strongDirection = 0;
							e.BeginStrongAttack (this, null);
							return;
						}
						if (i.IsLeftStickDown()) {
							fightingModel.strongDirection = 1;
							e.BeginStrongAttack(this, null);
							return;
						}
						if (i.IsLeftStickRight()) {
							fightingModel.strongDirection = 2;
							e.BeginStrongAttack(this, null);
							return;
						}
						if (i.IsLeftStickLeft()) {
							fightingModel.strongDirection = 3;
							e.BeginStrongAttack(this, null);
							return;
						}
					}
					if (i.GetRightTriggerHold ()) {
						e.OnBlock (this, null);
						return;
					}
					if (i.GetLeftTriggerHold ()) {
						e.OnBlast (this, null);
						return;
					}
				} else {
					if (i.GetButtonADown () && !dblJump) {
						rigid.AddForce (Vector3.up * 10f, ForceMode.VelocityChange);
						dblJump = true;
					}
				}
				if (i.GetButtonLBDown ()) {
					if (simonModel.isPinned)
						e.UnpinSimon (this, null);
				}
				if (i.GetButtonRBDown ()) {
					if (camModel != null) {
						Vector3 vel = rigid.velocity;
						vel.x = 0f;
						vel.z = 0f;
						rigid.velocity = vel;
						Vector3 forward = camModel.cameraTransform.TransformDirection (Vector3.forward);
						Vector3 right = camModel.cameraTransform.TransformDirection (Vector3.right);
						if (playerModel.isGrounded) {
							if (forward.y < 0f) {
								forward.y = 0f;
							}
						}
						Vector3 dir = (forward * playerModel.v + right * playerModel.h).normalized;
						rigid.AddForce (dir * 20f, ForceMode.VelocityChange);
					}
				}
				if (i.GetButtonBDown () && !fightingModel.isComboing && !fightingModel.isShooting) {
					e.OnProjectile (this, null);
				}
			}
		}
	}

	private void AddVelocity() {
		Vector3 vel = rigid.velocity;
		vel.y = 0;
		Vector3 targetVel = relativeMovement.normalized * playerModel.maxSpeed;
		if (playerModel.isGrounded)
			vel = Vector3.Lerp (vel, targetVel, playerModel.acceleration);
		else
			vel = Vector3.Lerp (vel, targetVel, playerModel.airAcceleration);
		vel.y = rigid.velocity.y;
		rigid.velocity = vel;
	}

	private void GroundPound() {
		Collider[] colliders = Physics.OverlapSphere (playerModel.playerTransform.position, 4f * fightingModel.chargeMultiplier);
		float force = fightingModel.strongInitialKnockback [fightingModel.strongDirection] * fightingModel.chargeMultiplier;
		Vector3 position = playerModel.playerTransform.position;
		float radius = 4f * fightingModel.chargeMultiplier;
		HashSet<FracturedObject> sourceSet = new HashSet<FracturedObject> ();
		foreach (Collider collider in colliders) {
			FracturedObject source = collider.gameObject.GetComponentInParent<FracturedObject> ();
			if (source != null)
				sourceSet.Add (source);
			if (collider.gameObject.layer == 9) {
				if (collider.gameObject.GetComponentInParent<Base> () != b) {
					collider.gameObject.GetComponent<Rigidbody> ().AddExplosionForce (force, position, radius, 0f, ForceMode.VelocityChange);
				}
			}
				

		}
		foreach (FracturedObject obj in sourceSet) {
			obj.Explode (position, force, radius, false, false, true, false);
		}
		game.assetStore.InstantiateObjectWithKey ("Explosion", transform.position + transform.forward, Quaternion.LookRotation(Vector3.up));

	}

	private CharacterModels playerModel {
		get {
			return b.m.pM;
		}
	}

	private CameraModel camModel {
		get {
			if (b.m.cM != null)
				return b.m.cM;
			//Debug.LogWarning ("'PlayerComponent' cannot retrieve 'CameraModel'");
			return null;
		}
	}

	private SimonModel simonModel {
		get {
			if (b.m.sM != null) {
				return b.m.sM;
			}
			Debug.LogWarning ("'PlayerComponent' cannot retrieve 'SimonModel'");
			return null;
		}
	}

	private FightingModel fightingModel {
		get {
			if (b.m.fM != null) {
				return b.m.fM;
			}
			return null;
		}
	}

	private bool isGrounded {
		get {
			if (simonModel.isPinned) {
				return Physics.SphereCast (new Ray (transform.position + Vector3.up, Vector3.down), 0.7f, 1f, playerModel.groundMask, QueryTriggerInteraction.Ignore);
			}
			else
				return Physics.SphereCast (new Ray (simonModel.ragdollRootTransform.position + Vector3.up, Vector3.down), 0.7f, 2f, playerModel.groundMask, QueryTriggerInteraction.Ignore);
		}
	}

	private Vector3 relativeMovement {
		get {
			Vector3 moveDirection;
			if (camModel) {
				Vector3 forward = b.m.cM.cameraTransform.TransformDirection (Vector3.forward);
				forward.y = 0f;
				Vector3 right = b.m.cM.cameraTransform.TransformDirection (Vector3.right);
				moveDirection = (b.m.pM.h * right + b.m.pM.v * forward).normalized;
				return moveDirection;
			} else
				return moveDirection = new Vector3 (playerModel.h, 0f, playerModel.v);
		}
	}

	private float velocity {
		get {
			return playerModel.relativeVelocity.magnitude;
		}
	}

	private Vector3 relativeVelocity {
		get {
			if (camModel != null) {
				return camModel.cameraTransform.InverseTransformDirection (rigid.velocity);
			} else
				return transform.InverseTransformDirection (rigid.velocity);
		}
	}

	private float acceleration {
		get {
			Vector3 dir = playerModel.relativeAcceleration;
			dir.y = 0f;
			if (dir.z < 0f)
				return -dir.magnitude;
			else
				return dir.magnitude;
		}
	}

	private Vector3 relativeAccceleration {
		get {
			Vector3 vel = relativeVelocity;
			if (vel.x < 0f)
				vel.x *= -1f;
			if (vel.z < 0f)
				vel.z *= -1f;
			Vector3 accel = (vel - lastVelocity)/Time.fixedDeltaTime;
			accelerationMag = vel.magnitude - lastVelocity.magnitude;
			lastVelocity = vel;
			return accel;
		}
	}

	private Quaternion targetRotation {
		get {
			Vector3 dir;
			if (camModel != null) {
				dir = camModel.cameraTransform.forward;
			} else
				dir = transform.forward;
			dir.y = 0f;
			Quaternion rot;
			if (dir != Vector3.zero)
				rot = Quaternion.LookRotation (dir, Vector3.up);
			else
				rot = transform.rotation;

			Vector3 tiltAxis = Vector3.Cross(Vector3.up, rigid.velocity.normalized);
			float lean = Mathf.Clamp (accelerationMag * 400f, -25f, 25f);
			lean = Mathf.Lerp (lastAcceleration, lean, Time.fixedDeltaTime);
			lastAcceleration = lean;
			Quaternion tilt = Quaternion.AngleAxis (lean, tiltAxis);
			return tilt * rot;

		}
	}

	//EventHandler Methods

	public void OnGroundEnter(object sender, object data) {
		dblJump = false;
		aim.active = true;
		if (rigid.velocity.y < -12f) {
			if (simonModel.isPinned)
				game.assetStore.InstantiateObjectWithKey ("Dust", transform.position, Quaternion.LookRotation (Vector3.up));
			else
				game.assetStore.InstantiateObjectWithKey ("Dust", simonModel.ragdollRootTransform.position, Quaternion.LookRotation (Vector3.up));
		}
	}

	public void OnGroundExit(object sender, object data) {
		aim.active = false;
	}

	public void UnpinSimon(object sender, object data) {
		aim.active = false;
		playerModel.fightingState = FightingState.Nuetral;
	}

	public void RepinSimon(object sender, object data) {
		transform.position = simonModel.ragdollRootTransform.position;
		rigid.velocity = Vector3.zero;
		playerModel.fightingState = FightingState.Nuetral;
	}

	public void WeakAttackSetup(object sender, object data) {
		aim.active = fightingModel.comboStep != 2;
		if (fightingModel.comboStep == 2) {
			Vector3 forward = playerModel.playerTransform.forward;
			forward.y = 0f;
			rigid.AddForce (forward.normalized * playerModel.weakForce, ForceMode.VelocityChange);
		}
	}

	public void BeginStrongAttack (object sender, object data) {
		aim.active = false;
	}

	public void EndStrongAttack(object sender, object data) {
		aim.active = true;
	}

	public void OnStrongAttackEvent(object sender, object data) {
		switch (fightingModel.strongDirection) {
		case 0:
			rigid.AddForce (playerModel.playerTransform.forward * 10f * fightingModel.chargeMultiplier, ForceMode.VelocityChange);
			break;
		case 1:
			rigid.AddForce (playerModel.playerTransform.forward * 5f * fightingModel.chargeMultiplier, ForceMode.VelocityChange);
			break;
		case 2:
			GroundPound ();
			break;
		case 3:
			GroundPound ();
			break;
		}
	}

	public void EndWeakCombo (object sender, object data) {
		aim.active = true;
	}

	public void OnWeakAttackEnter(object sender, object data) {
		/*if (fightingModel.comboStep == 2) {
			Vector3 forward = playerModel.playerTransform.forward;
			forward.y = 0f;
			rigid.AddForce (forward.normalized * playerModel.weakForce, ForceMode.VelocityChange);
		}*/
	}

	public void OnBlast(object sender, object data) {
		aim.active = false;
	}

	public void OnBlastExit(object sender, object data) {
		aim.active = true;
	}

	public void OnAttackReceived (Base enemy, AttackData data, Collision collision) {
		rigid.AddForce (data.direction * data.knockback, ForceMode.VelocityChange);
		game.assetStore.InstantiateObjectWithKey ("Hit", collision.contacts [0].point, Quaternion.identity);
		int style = Random.Range (1, 3);
		if (data.type == FightingState.Strong)
			game.audioManager.PlayAudio ("StrongHit" + style.ToString(), this, false);
		else
			game.audioManager.PlayAudio ("WeakHit" + style.ToString(), this, false);
	}

	public void OnStrong(object sender, object data) {
		aim.active = false;
	}

	public void OnStrongExit() {
		aim.active = true;
	}

	public override int priority {
		get {
			return 2;
		}
	}
}
