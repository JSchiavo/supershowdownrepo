﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseElement : GameElement, IComparable {

	[HideInInspector]
	public Base b;
	[HideInInspector]
	public ModelContainer m;
	public EventHandler e;
	public InputHandler i;

	protected bool initiate;

	public virtual void Initiate() {
		b = gameObject.GetComponentInParent<Base> ();
		e = b.e;
		i = b.i;
		m = b.m;
		SubscribeMethods ();
		initiate = true;
	}


	public virtual void SubscribeMethods() {
		if (e == null) {
			Debug.LogError ("No 'EventHandler' to subscribe to");
			return;
		}
	}

	public virtual int priority {
		get {
			return 100;
		}
	}
		
	public int CompareTo (object obj)
	{
		if (obj.GetType () != typeof(BaseElement))
			return -1;

		BaseElement element = (BaseElement)obj;

		return(this.priority.CompareTo(element.priority));
	}

}
