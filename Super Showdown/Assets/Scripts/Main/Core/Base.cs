﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;

public class Base : GameElement {
	//base properties
	private bool m_useKeyboard;
	private bool m_useController;

	//base components
	public EventHandler e;
	public InputHandler i;
	public ModelContainer m;
	public List<BaseElement> elements;

	public override void Start() {
		base.Start ();
		m = gameObject.GetComponentInChildren<ModelContainer> ();
		e = new EventHandler ();
		if (m == null) {
			Debug.LogError ("No 'ModelContainer' can be found within the Base gameObject");
			return;
		}
			
		if (XCI.GetNumPluggedCtrlrs () <= 0)
			useKeyboard = true;
		else
			useController = true;
	}

	void InitiateElements() {
		elements = new List<BaseElement>();
		foreach (BaseElement element in gameObject.GetComponentsInChildren<BaseElement>()) {
			elements.Add (element);
		}
		elements.Sort ();
		foreach (BaseElement element in elements) {
			element.Initiate ();
		}
	}

	public bool useKeyboard {
		get {
			return m_useKeyboard;
		}
		set {
			if (value) {
				m_useKeyboard = true;
				m_useController = false;

				i = new KeyboardInputHandler ();
				InitiateElements ();
			}
		}
	}

	public bool useController {
		get {
			return m_useController;
		}
		set {
			if (value) {
				if (XCI.GetNumPluggedCtrlrs () <= 0) {
					Debug.LogWarning ("No Xbox controllers are connected. Switching to Keyboard.");
					useKeyboard = true;
					return;
				}

				m_useKeyboard = false;
				m_useController = true;

				i = new XboxInputHandler ();
				InitiateElements ();
				XboxInputHandler t_i = (XboxInputHandler) i;
				t_i.player = m.pM.Player;
			}
		}
	}
}
