﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventHandler {
	public delegate void GameNotification(object sender, object data);
	public GameNotification OnGroundEnter;
	public GameNotification OnGroundExit;
	public GameNotification BeginWeakCombo;
	public GameNotification EndWeakCombo;
	public GameNotification WeakAttackSetup;
	public GameNotification BeginStrongAttack;
	public GameNotification EndStrongAttack;
	public GameNotification OnStrongAttackEvent;
	public GameNotification OnStrongAttackEnter;
	public GameNotification OnStrongAttackExit;
	public GameNotification OnWeakAttackEnter;
	public GameNotification OnWeakAttackExit;
	public GameNotification OnStrong;
	public GameNotification UnpinSimon;
	public GameNotification RepinSimon;
	public GameNotification OnProjectile;
	public GameNotification OnBlast;
	public GameNotification OnBlastEnter;
	public GameNotification OnBlastExit;
	public GameNotification OnBlock;
	public GameNotification OnBlockEnter;
	public GameNotification OnBlockExit;

	public delegate void CombatNotification (Base enemy, AttackData data, Collision collision);
	public CombatNotification OnAttackReceived;
}
