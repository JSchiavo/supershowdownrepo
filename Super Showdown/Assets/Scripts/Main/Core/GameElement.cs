﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameElement : MonoBehaviour {

	[HideInInspector]
	public GameManager game;

	// Use this for initialization
	public virtual void Start () {
		game = gameObject.GetComponentInParent<GameManager> ();
	}
}
