﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public GameElement[] gElements;
	public Base[] players;
	public AssetStore assetStore;
	public AudioManager audioManager;

	// Use this for initialization
	void Start () {
		SetUp ();
	}


	private void SetUp() {
		gElements = gameObject.GetComponentsInChildren<GameElement> ();
		players = gameObject.GetComponentsInChildren<Base> ();
		assetStore = gameObject.GetComponentInChildren<AssetStore> ();
		audioManager = gameObject.GetComponentInChildren<AudioManager> ();
		foreach (Base player in players) {
			player.e.OnAttackReceived += OnAttackReceived;
		}
	}

	public Base GetEnemy(Base player) {
		foreach(Base b in players) {
			if (b != player)
				return b;
		}
		return null;
	}

	private void OnAttackReceived(Base enemy, AttackData data, Collision collision) {
		if (data.type == FightingState.Strong)
			StartCoroutine (SlowMotion());
	}

	private IEnumerator SlowMotion() {
		float duration = 0.2f;
		duration += Time.time;

		while (duration > Time.time) {
			Time.timeScale = 0.2f;
			Time.fixedDeltaTime = 0.02f * Time.timeScale;
			yield return null;
		}
		Time.timeScale = 1.0f;
		Time.fixedDeltaTime = 0.02f;
	}

}
