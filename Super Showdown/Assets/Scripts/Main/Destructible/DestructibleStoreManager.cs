﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructibleStoreManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Collider[] colliders = gameObject.GetComponentsInChildren<Collider>();
		foreach (Collider col in colliders) {
			col.isTrigger = false;
		}

		Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer> ();
		foreach (Renderer renderer in renderers) {
			renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
		}

		gameObject.SetActive (false);
	}
}
