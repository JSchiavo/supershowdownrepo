﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructibleSpawn : GameElement {

	public GameObject destructable;
	private GameObject current;

	// Use this for initialization
	void Start () {
		base.Start ();
		current = GameObject.Instantiate<GameObject> (destructable, gameObject.transform.position, Quaternion.LookRotation(transform.forward, Vector3.up) * destructable.transform.rotation);
		current.SetActive (true);
		Vector3 parentScale = transform.localScale;
		Vector3 localScale = current.transform.localScale;
		localScale.x *= parentScale.x;
		localScale.y *= parentScale.y;
		localScale.z *= parentScale.z;
		current.transform.localScale = localScale;
		current.layer = 10;
		for (int i = 0; i < current.transform.childCount; i++) {
			current.transform.GetChild (i).gameObject.layer = 10;
		}
		Collider[] colliders = current.GetComponentsInChildren<Collider> ();
		foreach (Collider col in colliders) {
			col.isTrigger = false;
		}
		current.GetComponent<FracturedObject> ().OnDetach += OnDetach;
	}

	void OnDrawGizmosSelected() {
		Gizmos.color = Color.red;
		Gizmos.DrawLine (gameObject.transform.position, gameObject.transform.position + gameObject.transform.right * 5f);
		Gizmos.color = Color.green;
		Gizmos.DrawLine (gameObject.transform.position, gameObject.transform.position + gameObject.transform.up * 5f);
		Gizmos.color = Color.blue;
		Gizmos.DrawLine (gameObject.transform.position, gameObject.transform.position + gameObject.transform.forward * 5f);
	}

	void OnDetach (object sender, object data) {
		game.audioManager.PlayAudio ("WallCrash", data, true);
	}
}
