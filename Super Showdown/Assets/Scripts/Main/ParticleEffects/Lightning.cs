﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lightning : MonoBehaviour {

	ParticleSystem pS;
	public float maxTime;
	private float delay;
	[Range(0f, 1f)]
	public float incrementRate;
	// Use this for initialization
	void Start () {
		pS = gameObject.GetComponent<ParticleSystem> ();
		delay = Time.time + pS.main.startDelayMultiplier;
		maxTime += Time.time + pS.main.startDelayMultiplier;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time > delay) {
			if (maxTime > Time.time) {
				if (Time.frameCount % Mathf.RoundToInt (1f / incrementRate) == 0) {
					var em = pS.emission;
					em.rateOverTimeMultiplier = em.rateOverTimeMultiplier + 1;
				}
			}
		}
	}
}
