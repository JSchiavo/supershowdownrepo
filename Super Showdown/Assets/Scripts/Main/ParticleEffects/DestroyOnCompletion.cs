﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnCompletion : MonoBehaviour {

	ParticleSystem pS;

	// Use this for initialization
	void Start () {
		pS = gameObject.GetComponent<ParticleSystem> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!pS.IsAlive (true)) {
			Destroy (gameObject);
		}
	}

	public void Kill() {
		ParticleSystem.MainModule m = pS.main;
		m.loop = false;
	}
}
