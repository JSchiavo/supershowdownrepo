﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class AssetStore : MonoBehaviour {

	public Asset[] storeInspector;
	private Dictionary<string, GameObject> store;

	void Start() {
		store = new Dictionary<string, GameObject> ();
		foreach (Asset obj in storeInspector) {
			store.Add (obj.key, obj.obj);
		}
	}

	public GameObject InstantiateObjectWithKey(string key, Vector3 position, Quaternion rotation) {
		return (GameObject) GameObject.Instantiate (store[key], position, rotation);
	}

	public GameObject InstantiateObjectWithKey(string key, Vector3 position, Quaternion rotation, Transform parent) {
		return (GameObject) GameObject.Instantiate (store [key], position, rotation, parent);
	}

	public GameObject InstantiateObjectWithKey(string key, Transform parent, bool worldPositionStays) {
		return (GameObject) GameObject.Instantiate (store [key], parent, worldPositionStays);
	}
		
	[System.Serializable]
	public struct Asset {
		public string key;
		public GameObject obj;
	}

}
