﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackData {

	public FightingState type;
	public MuscleGroup groupSource;
	public Vector3 direction;
	public float knockback;
	public float permanentDamage;

	public AttackData (FightingState type, MuscleGroup source, Vector3 direction, float knockback, float permanentDamage) {
		this.type = type;
		this.groupSource = source;
		this.direction = direction;
		this.knockback = knockback;
		this.permanentDamage = permanentDamage;
	}
}
