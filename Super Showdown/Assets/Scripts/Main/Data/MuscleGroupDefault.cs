﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuscleGroupDefault {

	//Root
	public float rootPositionSpring;
	public float rootPositionDamper;
	public float rootPin;

	//Spine
	public float spinePositionSpring;
	public float spinePositionDamper;
	public float spinePin;

	//Head
	public float headPositionSpring;
	public float headPositionDamper;
	public float headPin;

	//Arms
	public float armsPositionSpring;
	public float armsPositionDamper;
	public float armsPin;

	//Legs
	public float legsPositionSpring;
	public float legsPositionDamper;
	public float legsPin;

	public MuscleGroupDefault(float rootPositionSpring, float rootPositionDamper, float rootPin,
		float spinePositionSpring, float spinePositionDamper, float spinePin,
		float headPositionSpring, float headPositionDamper, float headPin,
		float armsPositionSpring, float armsPositionDamper, float armsPin,
		float legsPositionSpring, float legsPositionDamper, float legsPin)
	{
		//Root
		this.rootPositionSpring = rootPositionSpring;
		this.rootPositionDamper = rootPositionDamper;
		this.rootPin = rootPin;

		//Spine
		this.spinePositionSpring = spinePositionSpring;
		this.spinePositionDamper = spinePositionDamper;
		this.spinePin = spinePin;

		//Head
		this.headPositionSpring = headPositionSpring;
		this.headPositionDamper = headPositionDamper;
		this.headPin = headPin;

		//Arms
		this.armsPositionSpring = armsPositionSpring;
		this.armsPositionDamper = armsPositionDamper;
		this.armsPin = armsPin;

		//Legs
		this.legsPositionSpring = legsPositionSpring;
		this.legsPositionDamper = legsPositionDamper;
		this.legsPin = legsPin;
	}

	public float[] GetMuscleGroupDefaults (MuscleGroup group) {
		float[] weights;
		switch (group) {
		case MuscleGroup.Arms:
			weights = new float[] { armsPositionSpring, armsPositionDamper, armsPin };
			break;
		case MuscleGroup.Head:
			weights = new float[] { headPositionSpring, headPositionDamper, headPin };
			break;
		case MuscleGroup.Legs:
			weights = new float[] { legsPositionSpring, legsPositionDamper, legsPin };
			break;
		case MuscleGroup.Root:
			weights = new float[] { rootPositionSpring, rootPositionDamper, rootPin };
			break;
		case MuscleGroup.Spine:
			weights = new float[] { spinePositionSpring, spinePositionDamper, spinePin };
			break;
		default:
			weights = new float[] { -1, -1, -1 };
			break;
		}
		return weights;
	}
}
