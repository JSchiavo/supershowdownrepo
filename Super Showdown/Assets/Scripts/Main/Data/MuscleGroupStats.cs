﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuscleGroupStats {
	public MuscleGroup group;
	private float m_health;
	private float m_permanentDamage;
	private float m_temporaryDamage;
	public float positionSpringTarget;
	public float positionDamperTarget;
	public float pinStrengthTarget;
	public float muscleMultiplierTarget;
	public bool isColliding;
	private bool m_isPinned;

	public MuscleGroupStats (MuscleGroup group) {
		this.group = group;
		this.m_health = 100f;
		this.m_permanentDamage = 0f;
		this.m_temporaryDamage = 0f;
		if (group == MuscleGroup.Legs || group == MuscleGroup.Root)
			this.pinStrengthTarget = 1f;
		else
			this.pinStrengthTarget = 0f;

		isPinned = true;
	}

	public bool isPinned {
		get {
			return m_isPinned;
		}
		set {
			if (!value) {
				if (group == MuscleGroup.Root) {
					positionSpringTarget = 100f;
					muscleMultiplierTarget = 1f;
					pinStrengthTarget = 0f;
				} else {
					positionSpringTarget = 1000f;
					muscleMultiplierTarget = 10f;
					pinStrengthTarget = 0f;
				}
			} else {
				pinStrengthTarget = 1f;
				positionSpringTarget = 1000f;
				positionDamperTarget = 0f;
				muscleMultiplierTarget = 1f;
			}
			m_isPinned = value;
		}
	}

	public float health {
		get {
			return Mathf.Clamp ((m_health - m_permanentDamage - m_temporaryDamage) / 100f, 0f, 1f);
		}
	}

	public float permanentDamage {
		get {
			return m_permanentDamage;
		}
	}

	public float temporaryDamage {
		get {
			return m_temporaryDamage;
		}
	}

	public void AddPermanentDamage(float damage) {
		m_permanentDamage = Mathf.Clamp (m_permanentDamage + damage, 0f, 100f);
	}

	public void AddTemporaryDamage(float damage) {
		m_temporaryDamage = Mathf.Clamp (m_temporaryDamage + damage, 0f, 100f);
	}

	public bool isDamaged {
		get {
			return m_temporaryDamage > 0f;
		}
	}
}
