﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SimonSays : MonoBehaviour {

	public 	GameObject	 Simon;

	public Transform root;
	public GameObject target;

	public List<Muscle> joints;
	public Collider[] muscleColliders;
	public MuscleEventHandler muscleHandler = new MuscleEventHandler();

	public bool[] show = new bool[5];
	public float[] spring = new float[5];
	public float[] damper = new float[5];
	public float[] pin = new float[5];
	public float[] muscleMultiplier = new float[5];

	public List<Muscle> rootMuscles;
	public List<Muscle> spineMuscles;
	public List<Muscle> headMuscles;
	public List<Muscle> armMuscles;
	public List<Muscle> legMuscles;

	public List<Muscle> rootPhysicsMuscles;
	public List<Muscle> spinePhysicsMuscles;
	public List<Muscle> headPhysicsMuscles;
	public List<Muscle> armPhysicsMuscles;
	public List<Muscle> legPhysicsMuscles;

	void Start() {
		muscleColliders = Simon.gameObject.GetComponentsInChildren<Collider> ();
		GroupMuscles ();
	}

	void FixedUpdate() {
		for (int i = 0; i < joints.Count; i++) {
			joints [i].UpdateJoint ();
		}
	}

	public void BuildSimon() {
		if (Simon.GetComponent<Animator> () == null) {
			Debug.LogError ("Cancelling build. Simon game object must have an Animator with valid Avatar attached.");
			DestroyImmediate (target);
			return;
		}

		target = (GameObject)Instantiate (Simon, gameObject.transform);
		DestroyImmediate (Simon.GetComponent<Animator> ());

		CleanTarget ();

		joints = new List<Muscle> ();
		SkinnedMeshRenderer ragdollRenderer = gameObject.GetComponentInChildren<SkinnedMeshRenderer> ();
		SkinnedMeshRenderer targetRenderer = target.GetComponentInChildren<SkinnedMeshRenderer> ();

		target.transform.localScale = Simon.transform.localScale;
		target.transform.localPosition = Simon.transform.localPosition;

		if (ragdollRenderer != null && targetRenderer != null) {
			ragdollRenderer.rootBone.localPosition = targetRenderer.rootBone.localPosition;
			root = ragdollRenderer.rootBone;
			BuildJoints (ragdollRenderer.rootBone, targetRenderer.rootBone);
		}
	}

	void CleanTarget() {
		Collider[] colliders = target.gameObject.GetComponentsInChildren<Collider> ();
		ConfigurableJoint[] targetJoints = target.gameObject.GetComponentsInChildren<ConfigurableJoint>();
		Rigidbody[] rigids = target.gameObject.GetComponentsInChildren<Rigidbody> ();
		Muscle[] targetMuscles = target.gameObject.GetComponentsInChildren<Muscle>();

		if (colliders.Length != 0) {
			foreach (Collider col in colliders) {
				DestroyImmediate (col);
			}
		}
		if (targetJoints.Length != 0) {
			foreach (ConfigurableJoint cj in targetJoints) {
				DestroyImmediate (cj);
			}
		}
		if (rigids.Length != 0) {
			foreach (Rigidbody rigid in rigids) {
				DestroyImmediate (rigid);
			}
		}
		if (targetMuscles.Length != 0) {
			foreach (Muscle muscle in targetMuscles) {
				DestroyImmediate (muscle);
			}
		}
	}

	void BuildJoints(Transform bone, Transform targetBone) {
		int children = bone.childCount;
		if (children > 0) {
			Muscle muscle = bone.gameObject.GetComponent<Muscle> () == null ? bone.gameObject.AddComponent<Muscle> () : bone.gameObject.GetComponent<Muscle> ();
			joints.Add (muscle);
			AssignMuscleToGroup (joints [joints.Count - 1], targetBone);
			muscle.SetUp (targetBone, this);
			for (int i = 0; i < children; i++) {
				BuildJoints (bone.GetChild (i), targetBone.GetChild (i));
			}
		}
	}

	public void Clean() {
		Muscle[] muscles = gameObject.GetComponentsInChildren<Muscle> ();
		ConfigurableJoint[] joints = gameObject.GetComponentsInChildren<ConfigurableJoint> ();
		foreach (Muscle muscle in muscles) {
			DestroyImmediate (muscle);
		}
		foreach (ConfigurableJoint joint in joints) {
			DestroyImmediate (joint);
		}
		DestroyImmediate (target);
	}

	public void UpdateMuscleGroup(MuscleGroup group, float positionSpring, float positionDamper, float pinStrength, float muscleMulti) {
		if (joints == null)
			return;
		if (group == MuscleGroup.All) {
			foreach (Muscle joint in joints) {
				if (joint != null)
					joint.UpdateWeights (positionSpring, positionDamper, pinStrength, muscleMulti);
			}
		} else {
			switch (group) {
			case MuscleGroup.Root:
				foreach (Muscle joint in rootMuscles) {
					joint.UpdateWeights (positionSpring, positionDamper, pinStrength, muscleMulti);
				}
				break;
			case MuscleGroup.Spine:
				foreach (Muscle joint in spineMuscles) {
					joint.UpdateWeights (positionSpring, positionDamper, pinStrength, muscleMulti);
				}
				break;
			case MuscleGroup.Head:
				foreach (Muscle joint in headMuscles) {
					joint.UpdateWeights (positionSpring, positionDamper, pinStrength, muscleMulti);
				}
				break;
			case MuscleGroup.Arms:
				foreach (Muscle joint in armMuscles) {
					joint.UpdateWeights (positionSpring, positionDamper, pinStrength, muscleMulti);
				}
				break;
			case MuscleGroup.Legs:
				foreach (Muscle joint in legMuscles) {
					joint.UpdateWeights (positionSpring, positionDamper, pinStrength, muscleMulti);
				}
				break;
			}
		}
	}

	public void UpdateMuscle(Muscle muscle, float positionSpring, float positionDamper, float pinStrength, float muscleMulti) {
		if (muscle.joint == null)
			return;
		muscle.UpdateWeights (positionSpring, positionDamper, pinStrength, muscleMulti);
	}

	void AssignMuscleToGroup(Muscle muscle, Transform targetBone) {
		Animator anim = target.GetComponent<Animator> ();
		Transform neck = anim.GetBoneTransform (HumanBodyBones.Neck);
		Transform spine = anim.GetBoneTransform (HumanBodyBones.Spine);
		Transform root = anim.GetBoneTransform (HumanBodyBones.Hips);
		Transform leftArm = anim.GetBoneTransform (HumanBodyBones.LeftShoulder);
		Transform rightArm = anim.GetBoneTransform (HumanBodyBones.RightShoulder);
		Transform leftLeg = anim.GetBoneTransform (HumanBodyBones.LeftUpperLeg);
		Transform rightLeg = anim.GetBoneTransform (HumanBodyBones.RightUpperLeg);

		Transform currentBone = targetBone;
		if (currentBone == root) {
			muscle.group = MuscleGroup.Root;
			return;
		} else {
			while (currentBone.parent != null) {
				if (currentBone == spine) {
					muscle.group = MuscleGroup.Spine;
					return;
				}
				if (currentBone == leftLeg) {
					muscle.group = MuscleGroup.Legs;
					return;
				}
				if (currentBone == rightLeg) {
					muscle.group = MuscleGroup.Legs;
					return;
				}
				if (currentBone == neck) {
					muscle.group = MuscleGroup.Head;
					return;
				}
				if (currentBone == leftArm) {
					muscle.group = MuscleGroup.Arms;
					return;
				}
				if (currentBone == rightArm) {
					muscle.group = MuscleGroup.Arms;
					return;
				}
				currentBone = currentBone.parent;
			}
		}
	}

	void GroupMuscles() {
		rootMuscles = new List<Muscle> ();
		spineMuscles = new List<Muscle> ();
		headMuscles = new List<Muscle> ();
		armMuscles = new List<Muscle> ();
		legMuscles = new List<Muscle> ();

		rootPhysicsMuscles = new List<Muscle> ();
		spinePhysicsMuscles = new List<Muscle> ();
		headPhysicsMuscles = new List<Muscle> ();
		armPhysicsMuscles = new List<Muscle> ();
		legPhysicsMuscles = new List<Muscle> ();

		foreach (Muscle muscle in joints) {
			switch (muscle.group) {
			case MuscleGroup.Root:
				rootMuscles.Add (muscle);
				if (muscle.usePhysics)
					rootPhysicsMuscles.Add (muscle);
				break;
			case MuscleGroup.Spine:
				spineMuscles.Add (muscle);
				if (muscle.usePhysics)
					spinePhysicsMuscles.Add (muscle);
				break;
			case MuscleGroup.Head:
				headMuscles.Add (muscle);
				if (muscle.usePhysics)
					headPhysicsMuscles.Add (muscle);
				break;
			case MuscleGroup.Arms:
				armMuscles.Add (muscle);
				if (muscle.usePhysics)
					armPhysicsMuscles.Add (muscle);
				break;
			case MuscleGroup.Legs:
				legMuscles.Add (muscle);
				if (muscle.usePhysics)
					legPhysicsMuscles.Add (muscle);
				break;
			}
		}
	}

	public void AddForceToMuscleGroup(MuscleGroup group, Vector3 force, ForceMode type) {
		switch (group) {
		case MuscleGroup.All:
			AddForceToMuscleGroup (MuscleGroup.Root, force, type);
			break;
		case MuscleGroup.Root:
			foreach (Muscle muscle in rootPhysicsMuscles) {
				muscle.rigid.AddForce (force, type);
			}
			foreach (Muscle muscle in spinePhysicsMuscles) {
				muscle.rigid.AddForce (force, type);
			}
			foreach (Muscle muscle in headPhysicsMuscles) {
				muscle.rigid.AddForce (force, type);
			}
			foreach (Muscle muscle in armPhysicsMuscles) {
				muscle.rigid.AddForce (force, type);
			}
			foreach (Muscle muscle in legPhysicsMuscles) {
				muscle.rigid.AddForce (force, type);
			}
			break;
		case MuscleGroup.Spine:
			foreach (Muscle muscle in spinePhysicsMuscles) {
				muscle.rigid.AddForce (force, type);
			}
			foreach (Muscle muscle in headPhysicsMuscles) {
				muscle.rigid.AddForce (force, type);
			}
			foreach (Muscle muscle in armPhysicsMuscles) {
				muscle.rigid.AddForce (force, type);
			}
			break;
		case MuscleGroup.Head:
			foreach (Muscle muscle in headPhysicsMuscles) {
				muscle.rigid.AddForce (force, type);
			}
			foreach (Muscle muscle in spinePhysicsMuscles) {
				muscle.rigid.AddForce (force, type);
			}
			foreach (Muscle muscle in armPhysicsMuscles) {
				muscle.rigid.AddForce (force, type);
			}
			break;
		case MuscleGroup.Arms:
			foreach (Muscle muscle in armPhysicsMuscles) {
				muscle.rigid.AddForce (force, type);
			}
			break;
		case MuscleGroup.Legs:
			foreach (Muscle muscle in legPhysicsMuscles) {
				muscle.rigid.AddForce (force, type);
			}
			foreach (Muscle muscle in rootPhysicsMuscles) {
				muscle.rigid.AddForce (force, type);
			}
			break;
		}
	}

	public void AddTorqueToMuscleGroup(MuscleGroup group, Vector3 force, ForceMode type) {
		switch (group) {
		case MuscleGroup.All:
			AddTorqueToMuscleGroup (MuscleGroup.Root, force, type);
			AddTorqueToMuscleGroup (MuscleGroup.Spine, force, type);
			AddTorqueToMuscleGroup (MuscleGroup.Head, force, type);
			AddTorqueToMuscleGroup (MuscleGroup.Arms, force, type);
			AddTorqueToMuscleGroup (MuscleGroup.Legs, force, type);
			break;
		case MuscleGroup.Root:
			foreach (Muscle muscle in rootPhysicsMuscles) {
				muscle.rigid.AddTorque (force, type);
			}
			foreach (Muscle muscle in spinePhysicsMuscles) {
				muscle.rigid.AddTorque (force, type);
			}
			foreach (Muscle muscle in headPhysicsMuscles) {
				muscle.rigid.AddTorque(force, type);
			}
			foreach (Muscle muscle in armPhysicsMuscles) {
				muscle.rigid.AddTorque (force, type);
			}
			break;
		case MuscleGroup.Spine:
			foreach (Muscle muscle in spinePhysicsMuscles) {
				muscle.rigid.AddTorque (force, type);
			}
			foreach (Muscle muscle in headPhysicsMuscles) {
				muscle.rigid.AddTorque(force, type);
			}
			foreach (Muscle muscle in armPhysicsMuscles) {
				muscle.rigid.AddTorque (force, type);
			}
			break;
		case MuscleGroup.Head:
			foreach (Muscle muscle in headPhysicsMuscles) {
				muscle.rigid.AddTorque(force, type);
			}
			foreach (Muscle muscle in spinePhysicsMuscles) {
				muscle.rigid.AddTorque (force, type);
			}
			break;
		case MuscleGroup.Arms:
			foreach (Muscle muscle in armPhysicsMuscles) {
				muscle.rigid.AddTorque (force, type);
			}
			break;
		case MuscleGroup.Legs:
			foreach (Muscle muscle in legPhysicsMuscles) {
				muscle.rigid.AddTorque (force, type);
			}
			break;
		}
	}

	public Muscle[] muscles {
		get {
			return joints.ToArray();
		}
	}

	public Muscle[] GetMuscleGroup(MuscleGroup group) {
		if (group != MuscleGroup.All) {
			List<Muscle> x = new List<Muscle> ();
			foreach (Muscle muscle in joints) {
				if (muscle.group == group)
					x.Add (muscle);
			}
			return x.ToArray ();
		} else
			return muscles;
	}

	public Muscle GetMuscleByTarget(Transform target) {
		foreach (Muscle muscle in joints) {
			if (muscle.target == target)
				return muscle;
		}
		return null;
	}

	public void SetWeights(MuscleGroup group) {

		if (MuscleGroup.All == group) {
			SetWeights (MuscleGroup.Root);
			SetWeights (MuscleGroup.Spine);
			SetWeights (MuscleGroup.Head);
			SetWeights (MuscleGroup.Arms);
			SetWeights (MuscleGroup.Legs);
			return;
		} else {
			switch (group) {
			case MuscleGroup.Root:
				UpdateMuscleGroup (group, spring [0], damper [0], pin [0], muscleMultiplier [0]);
				break;
			case MuscleGroup.Spine:
				UpdateMuscleGroup (group, spring [1], damper [1], pin [1], muscleMultiplier [1]);
				break;
			case MuscleGroup.Head:
				UpdateMuscleGroup (group, spring [2], damper [2], pin [2], muscleMultiplier [2]);
				break;
			case MuscleGroup.Arms:
				UpdateMuscleGroup (group, spring [3], damper [3], pin [3], muscleMultiplier [3]);
				break;
			case MuscleGroup.Legs:
				UpdateMuscleGroup (group, spring [4], damper [4], pin [4], muscleMultiplier [4]);
				break;
			}
		}
	}
}
