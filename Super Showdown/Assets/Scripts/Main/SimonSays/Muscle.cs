﻿using UnityEngine;
using System.Collections;

public class Muscle : MonoBehaviour {

	[SerializeField]
	private SimonSays simon;
	public ConfigurableJoint joint;
	public Transform target;
	public MuscleGroup group;
	public Rigidbody rigid;
	public bool usePhysics;
	[SerializeField]
	private Muscle parent;
	public Quaternion jointSpace, jointSpaceInverse, targetAnimatedRotation, parentSpace, startLocalRotation, localRotationConvert, startJointRotation;
	public float pin;
	public JointDrive slerpDrive = new JointDrive();
	private Collider collider;

	void Start() {
		collider = gameObject.GetComponent<Collider> ();
	}

	// Update is called once per frame
	public void UpdateJoint () {
		if (usePhysics) {
			if (rigid.IsSleeping ()) {
				rigid.WakeUp ();
			}
		}
		if (target != null) {
			if (usePhysics) {
				UpdateRotation ();

			} else {
				transform.localRotation = target.localRotation;
			}
			PinMuscle ();
		}

	}

	public void SetUp(Transform targetBone, SimonSays simon) {
		this.simon = simon;
		target = targetBone;
		DetermineIfUsePhysics ();
		parentSpace = Quaternion.Inverse (targetParentRotation) * parentRotation;

		if (usePhysics) {
			rigid = gameObject.GetComponent<Rigidbody> () != null ? gameObject.GetComponent<Rigidbody> () : gameObject.AddComponent<Rigidbody> ();
			joint = gameObject.GetComponent<ConfigurableJoint> () != null ? gameObject.GetComponent<ConfigurableJoint> () : gameObject.AddComponent<ConfigurableJoint> ();
			joint.autoConfigureConnectedAnchor = true;
			if (simon.root != transform) {
				joint.xMotion = ConfigurableJointMotion.Locked;
				joint.yMotion = ConfigurableJointMotion.Locked;
				joint.zMotion = ConfigurableJointMotion.Locked;
			} else {
				joint.xMotion = ConfigurableJointMotion.Free;
				joint.yMotion = ConfigurableJointMotion.Free;
				joint.zMotion = ConfigurableJointMotion.Free;
			}
			joint.axis = new Vector3 (1, 0, 0);
			joint.secondaryAxis = new Vector3 (0, 1, 0);
			joint.rotationDriveMode = RotationDriveMode.Slerp;
			joint.connectedBody = gameObject.transform.parent != null ? gameObject.transform.parent.GetComponent<Rigidbody> () : null;
			if (transform != simon.root && transform.parent != null)
				parent = transform.parent.gameObject.GetComponent<Muscle> ();
			CalculateWeight ();

			startLocalRotation = muscleLocalRotation;

			Vector3 forward = Vector3.Cross (joint.axis, joint.secondaryAxis).normalized;
			Vector3 up = Vector3.Cross (forward, joint.axis).normalized;

			jointSpace = Quaternion.LookRotation (forward, up);
			jointSpaceInverse = Quaternion.Inverse (jointSpace);
			startJointRotation = startLocalRotation * jointSpace;
		}
		else {
			rigid = gameObject.GetComponent<Rigidbody> ();
			if (rigid != null) {
				DestroyImmediate (rigid);
			}
		}
			
		localRotationConvert = Quaternion.Inverse (targetLocalRotation) * muscleLocalRotation;
	}

	private void CalculateWeight() {
		Transform papa = transform;
		int distance = 1;
		while (papa != simon.root && papa != null) {
			distance++;
			papa = papa.parent;
		}
		rigid.angularDrag = 0.05f; //1f * distance;
		rigid.mass = 1f;
	}

	private void DetermineIfUsePhysics() {
		Animator anim = simon.target.GetComponent<Animator> ();
		int distance = 0;
		Transform currentBone = target;
		switch (this.group) {
		case MuscleGroup.Arms:
			Transform leftArm = anim.GetBoneTransform (HumanBodyBones.LeftShoulder);
			Transform rightArm = anim.GetBoneTransform (HumanBodyBones.RightShoulder);
			while ((currentBone != leftArm && currentBone != rightArm)) {
				currentBone = currentBone.parent;
				distance++;
			}
			usePhysics = (distance < 3);
			break;
		case MuscleGroup.Legs:
			Transform leftLeg = anim.GetBoneTransform (HumanBodyBones.LeftUpperLeg);
			Transform rightLeg = anim.GetBoneTransform (HumanBodyBones.RightUpperLeg);
			while ((currentBone != leftLeg && currentBone != rightLeg)) {
				currentBone = currentBone.parent;
				distance++;
			}
			usePhysics = (distance < 2);
			break;
		default:
			usePhysics = true;
			break;
		}
	}

	public void UpdateWeights(float spring, float damper, float pin, float power) {
		if (usePhysics) {
			float s;
			float d;
			s = spring;
			d = damper;
			if (joint.connectedBody == null && simon.root != transform) {
				s = 0f;
				d = 0f;
			} else
				s *= power;

			slerpDrive.positionSpring = s;
			slerpDrive.positionDamper = d;
			slerpDrive.maximumForce = s;
			joint.slerpDrive = slerpDrive;
			this.pin = pin;
		}
	}

	private void UpdateRotation() { 
		if (transform != simon.root) {
			targetAnimatedRotation = targetLocalRotation * localRotationConvert;
		} else {
			targetAnimatedRotation = target.rotation;
		}
		if (targetAnimatedRotation != joint.targetRotation)
			joint.targetRotation = LocalToJointSpace (targetAnimatedRotation);
	}

	private void PinMuscle() {
		if (usePhysics) {
			Vector3 positionOffset = target.position - rigid.position;
			Vector3 power = positionOffset / Time.fixedDeltaTime;
			Vector3 force = -rigid.velocity + power;
			force *= pin;
			force /= 1f + positionOffset.sqrMagnitude * 5f;
			rigid.velocity += force;

		} else {
			transform.localPosition = target.localPosition;
		}
	}

	private Quaternion parentRotation {
		get {
			if (joint != null) {
				if (joint.connectedBody != null)
					return joint.connectedBody.rotation;
				else if (transform.parent != null)
					return transform.parent.rotation;
			}
			else if (transform.parent != null)
				return transform.parent.rotation;


			return Quaternion.identity;

		}
	}

	private Quaternion targetParentRotation {
		get {
			if (parent != null) {
				return parent.target.rotation;
			} else if (target.parent != null)
				return target.parent.rotation;
			else
				return Quaternion.identity;

		}
	}

	private Quaternion muscleLocalRotation {
		get {
			return Quaternion.Inverse (parentRotation) * transform.rotation;
		}
	}

	private Quaternion targetLocalRotation {
		get {
			return Quaternion.Inverse (targetParentRotation * parentSpace) * target.rotation;
		}
	}

	private Quaternion LocalToJointSpace (Quaternion localRotation) {
		return jointSpaceInverse * Quaternion.Inverse (localRotation) * startJointRotation;
	}

	private bool IsCollisionWithMuscle(Collision collision) {
		foreach (Collider col in simon.muscleColliders) {
			if (collision.collider == col && collision.collider != collider) {
				return true;
			}
		}
		return false;
	}

	void OnCollisionEnter(Collision collision) {
		if (!IsCollisionWithMuscle (collision)) {
			if (simon.muscleHandler.OnMuscleCollisionEnter != null)
				simon.muscleHandler.OnMuscleCollisionEnter (this, collision);
		}
	}

	void OnCollisionStay(Collision collision) {
		if (!IsCollisionWithMuscle (collision)) {
			if (simon.muscleHandler.OnMuscleCollisionStay != null) {
				simon.muscleHandler.OnMuscleCollisionStay (this, collision);
			}
		}
	}

	void OnCollisionExit(Collision collision) {
		if (!IsCollisionWithMuscle (collision)) {
			if (simon.muscleHandler.OnMuscleCollisionExit != null) {
				simon.muscleHandler.OnMuscleCollisionExit (this, collision);
			}
		}
	}

}
