﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuscleEventHandler {

	public delegate void MuscleNotification (Muscle sender, Collision collision);
	public MuscleNotification OnMuscleCollisionEnter;
	public MuscleNotification OnMuscleCollisionStay;
	public MuscleNotification OnMuscleCollisionExit;
}
