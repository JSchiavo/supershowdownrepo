﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MuscleGroup {
	Root,
	Spine,
	Head,
	Arms,
	Legs,
	All
}
