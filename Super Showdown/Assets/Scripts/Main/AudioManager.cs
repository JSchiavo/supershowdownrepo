﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : GameElement {

	public float audioFalloff;
	public int audioSourcePoolSize;
	public AudioAsset[] audioInspector;
	private Queue audioSourcePool;
	private List<AudioSource> activeAudioSources;
	private Dictionary<string, AudioClip> audioStore;

	// Use this for initialization
	public override void Start () {
		base.Start ();
		audioStore = new Dictionary<string, AudioClip> ();
		foreach (AudioAsset aud in audioInspector) {
			audioStore.Add (aud.key, aud.audio);
		}
		CreateAudioSourcePool ();
	}

	private void CreateAudioSourcePool() {
		audioSourcePool = new Queue ();
		activeAudioSources = new List<AudioSource> ();
		for (int i = 0; i < audioSourcePoolSize; i++) {
			AudioSource source = gameObject.AddComponent<AudioSource> ();
			source.enabled = false;
			audioSourcePool.Enqueue (source);
		}
	}

	public void PlayAudio (string key, object sender, bool relativeToPlayer) {
		StartCoroutine(Play(key, sender, relativeToPlayer));
	}

	private IEnumerator Play(string key, object sender, bool relativeToPlayer) {
		AudioSource source;
		AudioClip clip;
		float timeUntilCompletion;
		Vector3 position = Vector3.zero;
		if (relativeToPlayer) {
			MonoBehaviour mono;
			if (sender is MonoBehaviour) {
				mono = (MonoBehaviour)sender;
				position = mono.gameObject.transform.position;
			}
		}

		if (audioSourcePool.Count == 0) {
			source = activeAudioSources [0];
			activeAudioSources.RemoveAt (0);
			source.Stop ();
			while (source.isPlaying)
				yield return null;
		} else {
			source = (AudioSource)audioSourcePool.Dequeue ();
		}
		source.enabled = true;

		activeAudioSources.Add (source);

		clip = audioStore [key];
		timeUntilCompletion = Time.time + clip.length;

		if (relativeToPlayer) {
			float volume = relativeVolume (position);
			source.PlayOneShot (clip, volume);
		}
		else
			source.PlayOneShot (clip);

		while (timeUntilCompletion > Time.time && source.isPlaying) {
			yield return null;
		}

		source.enabled = false;
		activeAudioSources.Remove (source);
		audioSourcePool.Enqueue (source);
	}

	public float relativeVolume(Vector3 position) {
		float distance = int.MaxValue;
		foreach (Base player in game.players) {
			float playerDistance = Vector3.Distance (position, player.m.pM.playerTransform.position);
			if (playerDistance < distance)
				distance = playerDistance;
		}
		distance = Mathf.Clamp (distance, 0f, audioFalloff);
		return ((audioFalloff + 1) - distance) / audioFalloff;
	}

	[System.Serializable]
	public struct AudioAsset {
		public string key;
		public AudioClip audio;
	}
}
