﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraModel : BaseElement {

	public Transform cameraTransform;
	[Range(1f, 20f)]
	public float distance;
	[Range(1f, 10f)]
	public float sensitivity;
	[Range(-180, 0)]
	public float minAngleX;
	[Range(0, 180)]
	public float maxAngleX;
	public float v;
	public float h;

	public override void Initiate() {
		base.Initiate ();
		distance = 8f;
		sensitivity = 3f;
		minAngleX = -15f;
		maxAngleX = 80f;
	}

	public override int priority {
		get {
			return 0;
		}
	}
}
