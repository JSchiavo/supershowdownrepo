﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelContainer : BaseElement {

	public CharacterModels pM;
	public CameraModel cM;
	public SimonModel sM;
	public FightingModel fM;

	public override void Initiate ()
	{
		base.Initiate ();
		pM = gameObject.GetComponent<CharacterModels> () != null ? gameObject.GetComponent<CharacterModels> () : gameObject.AddComponent<CharacterModels> ();
		cM = gameObject.GetComponent<CameraModel> (); //!= null ? gameObject.GetComponent<CameraModel> () : gameObject.AddComponent<CameraModel> ();
		sM = gameObject.GetComponent<SimonModel> () != null ? gameObject.GetComponent<SimonModel> () : gameObject.AddComponent<SimonModel> ();
		fM = gameObject.GetComponent<FightingModel> () != null ? gameObject.GetComponent<FightingModel> () : gameObject.AddComponent<FightingModel> ();
	}

	public override int priority {
		get {
			return 1;
		}
	}
}
