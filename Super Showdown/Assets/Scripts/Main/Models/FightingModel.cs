﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightingModel : BaseElement {

	public bool hasHit;
	public bool canCombo;
	public bool isMobile;
	public int comboStep = -1;
	public int strongDirection;

	public int[] weakAttackLayer = new int[3];
	public string[] weakAttackTags = new string[3];
	public MuscleGroup[] weakAttackMuscleGroup = new MuscleGroup[3];
	[Range(0f, 1f)]
	public float[] weakComboAllowedAfter = new float[3];
	[Range(0f, 1f)]
	public float[] weakComboAllowedBefore = new float[3];
	[Range(0f, 1f)]
	public float[] weakAttackEnter = new float[3];
	[Range(0f, 1f)]
	public float[] weakAttackExit = new float[3];
	[Range(0f, 30f)]
	public float[] weakKnockback = new float[3];
	[Range(0f, 5f)]
	public float[] weakPermanentDamage = new float[3];

	//Index 0 is Up, 1 is Down, 2 is Right, and 3 is Left
	public string[] strongAttackTags = new string[4];
	public MuscleGroup[] strongAttackMuscleGroup = new MuscleGroup[4];
	[Range(0f, 1f)]
	public float[] chargeChanceAt = new float[4];
	[Range(0f, 1f)]
	public float[] strongEventAt = new float[4];
	[Range(0f, 1f)]
	public float[] StrongAttackEnter = new float[4];
	[Range(0f, 1f)]
	public float[] strongAttackExit = new float[4];
	[Range(10f, 100f)]
	public float[] strongInitialKnockback = new float[4];
	[Range(1f, 10f)]
	public float[] strongInitialPermanentDamage = new float[4];
	public float chargeMultiplier;
	public float maxChargeTime;

	public string projectileTag;
	[Range(0f, 1f)]
	public float projectileShootAt;
	[Range(0f, 1f)]
	public float projectileRechargeAt;
	public bool isShooting;

	public string blockTag;
	[Range(0f, 1f)]
	public float blockEnter;
	[Range(0f, 1f)]
	public float blockExit;
	[Range(0f, 1f)]
	public float blockChance;

	public string blastTag;
	[Range(0f, 1f)]
	public float blastEnter;
	[Range(0f, 1f)]
	public float blastExit;


	public bool isComboing {
		get {
			return comboStep > -1;
		}
	}

	public bool isStrong {
		get {
			return strongDirection != -1;
		}
	}
}
