﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimonModel : BaseElement {

	public Transform ragdollRootTransform;
	public Transform targetRootTransform;
	public LayerMask collisionMask;

	[Range(1f, 5f)]
	public float unpinDistanceThreshold;
	[Range(0f, 5f)]
	public float recoveryVelocity;
	[Range(0f, 3f)]
	public float recoveryTimer;
	[Range(1f, 10f)]
	public float impactRadius;
	[Range(0f, 1f)]
	public float muscleBlend;
	[Range(0f, 10f)]
	public float tempDamageRecovery;

	private Dictionary<MuscleGroup, MuscleGroupStats> m_muscleGroupStats;

	public bool isPinned;
	public float impactDistance;

	public override void Initiate ()
	{
		base.Initiate ();
		isPinned = true;
		m_muscleGroupStats = new Dictionary<MuscleGroup, MuscleGroupStats> ();
		m_muscleGroupStats.Add (MuscleGroup.Root, new MuscleGroupStats (MuscleGroup.Root));
		m_muscleGroupStats.Add (MuscleGroup.Spine, new MuscleGroupStats (MuscleGroup.Spine));
		m_muscleGroupStats.Add (MuscleGroup.Head, new MuscleGroupStats (MuscleGroup.Head));
		m_muscleGroupStats.Add (MuscleGroup.Arms, new MuscleGroupStats (MuscleGroup.Arms));
		m_muscleGroupStats.Add (MuscleGroup.Legs, new MuscleGroupStats (MuscleGroup.Legs));
	}

	public override int priority {
		get {
			return 0;
		}
	}

	public MuscleGroupStats[] muscleGroupStats {
		get {
			List<MuscleGroupStats> stats = new List<MuscleGroupStats>();
			foreach (MuscleGroupStats stat in m_muscleGroupStats.Values)
				stats.Add (stat);
			return stats.ToArray ();
		}
	}

	public MuscleGroupStats GetMuscleGroupStats (MuscleGroup group) {
		if (group == MuscleGroup.All)
			return null;
		return m_muscleGroupStats [group];
	}
}
