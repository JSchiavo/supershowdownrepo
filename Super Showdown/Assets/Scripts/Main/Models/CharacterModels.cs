﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;

public class CharacterModels: BaseElement {

	public XboxController Player;
	public Transform playerTransform;
	[Range(0f, 20f)]
	public float maxSpeed;
	[Range(0f, 1f)]
	public float acceleration;
	[Range(0f, 1f)]
	public float turnSpeed;
	[Range(0f, 1f)]
	public float power;
	[Range(0f, 1f)]
	public float defense;
	[Range(1f, 100f)]
	public float fallGravity;
	[Range(1f, 100f)]
	public float floatGravity;
	[Range(0f, 1f)]
	public float airAcceleration;
	[Range(1f, 10f)]
	public float fallThreshold;
	[Range(0f, 10f)]
	public float weakForce;
	public LayerMask groundMask;
	public bool m_grounded;
	public float v;
	public float h;
	public Vector3 m_relativeAcceleration;
	public Vector3 m_relativeVelocity;
	public Vector3 relativeVelocity {
		get {
			return m_relativeVelocity;
		}
		set {
			m_relativeVelocity = Vector3.Lerp (m_relativeVelocity, value, 0.8f);
		}
	}
	public Vector3 relativeAcceleration {
		get {
			return m_relativeAcceleration;
		}
		set {
			m_relativeAcceleration = value;
		}
	}
	public float velocityY {
		get {
			return m_relativeVelocity.y;
		}
	}
	public bool isGrounded {
		get {
			return m_grounded;
		}
		set {
			if (value != m_grounded) {
				if (value)
					e.OnGroundEnter (this, null);
				else
					e.OnGroundExit (this, null);
				m_grounded = value;
			}
		}
	}

	public override void Initiate ()
	{
		base.Initiate ();
		fightingState = FightingState.Nuetral;
		hasHit = false;
	}


	public void HeavyConfigurations() {
		maxSpeed = 14f;
		acceleration = Time.deltaTime;
		power = 0.9f;
		defense = 0.8f;
		fallGravity = 100f;
		floatGravity = 20f;
		airAcceleration = Time.fixedDeltaTime;
	}

	public override int priority {
		get {
			return 0;
		}
	}

	//Combat Properties
	public bool hasHit;
	public FightingState fightingState;
	public AttackData currentAttack;

}
