﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;

public class XboxInputHandler : InputHandler {

	private Dictionary<string, string> controls;

	public override bool GetButtonADown ()
	{
		throw new System.NotImplementedException ();
	}

	public override bool GetButtonAHold ()
	{
		throw new System.NotImplementedException ();
	}

	public override bool GetButtonBDown ()
	{
		throw new System.NotImplementedException ();
	}

	public override bool GetButtonXDown ()
	{
		throw new System.NotImplementedException ();
	}

	public override bool GetButtonYDown ()
	{
		throw new System.NotImplementedException ();
	}

	public override bool GetButtonRBDown ()
	{
		throw new System.NotImplementedException ();
	}

	public override bool GetButtonLBDown ()
	{
		throw new System.NotImplementedException ();
	}

	public override bool GetLeftStickHold ()
	{
		throw new System.NotImplementedException ();
	}

	public override float GetLeftStickH ()
	{
		throw new System.NotImplementedException ();
	}

	public override float GetLeftStickV ()
	{
		throw new System.NotImplementedException ();
	}

	public override bool GetButtonYHold ()
	{
		throw new System.NotImplementedException ();
	}

	public override float GetRightStickH ()
	{
		throw new System.NotImplementedException ();
	}

	public override float GetRightStickV ()
	{
		throw new System.NotImplementedException ();
	}

	public override bool IsLeftStickLeft ()
	{
		throw new System.NotImplementedException ();
	}

	public override bool IsLeftStickRight ()
	{
		throw new System.NotImplementedException ();
	}

	public override bool IsLeftStickUp ()
	{
		throw new System.NotImplementedException ();
	}

	public override bool IsLeftStickDown ()
	{
		throw new System.NotImplementedException ();
	}

	public override bool GetRightStickPress ()
	{
		throw new System.NotImplementedException ();
	}

	public override bool GetLeftTriggerHold ()
	{
		throw new System.NotImplementedException ();
	}

	public override bool GetRightTriggerHold ()
	{
		throw new System.NotImplementedException ();
	}

	public XboxController player {
		set {
			controls = new Dictionary<string, string> ();
			if (value == XboxController.First) {
				controls.Add ("A", "A");
				controls.Add ("X", "X");
				controls.Add ("Y", "Y");
				controls.Add ("B", "B");
				controls.Add ("LB", "LB");
				controls.Add ("L3", "L3");
				controls.Add ("R3", "R3");
				controls.Add ("RB", "RB");
				controls.Add ("Start", "Start");
				controls.Add ("Horizontal", "Horizontal");
				controls.Add ("Vertical", "Vertical");
			} else if (value == XboxController.Second) {
				controls.Add ("A", "A_2");
				controls.Add ("X", "X_2");
				controls.Add ("Y", "Y_2");
				controls.Add ("B", "B_2");
				controls.Add ("LB", "LB_2");
				controls.Add ("L3", "L3_2");
				controls.Add ("R3", "R3_2");
				controls.Add ("RB", "RB_2");
				controls.Add ("Start", "Start_2");
				controls.Add ("Horizontal", "Horizontal_2");
				controls.Add ("Vertical", "Vertical_2");
			}
		}
	}

}
