﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardInputHandler : InputHandler {

	public bool isMac = true;

	void OnEnable() {
		RuntimePlatform platform = Application.platform;
		isMac = (platform == RuntimePlatform.OSXEditor || platform == RuntimePlatform.OSXEditor || platform == RuntimePlatform.OSXDashboardPlayer);
	}

	public override bool GetButtonADown ()
	{
		return Input.GetKeyDown (KeyCode.Space);
	}
	public override bool GetButtonAHold ()
	{
		return Input.GetKey (KeyCode.Space);
	}
	public override bool GetButtonBDown ()
	{
		return Input.GetKeyDown (KeyCode.E);
	}
	public override bool GetButtonLBDown ()
	{
		return Input.GetKeyDown (KeyCode.Q);
	}
	public override bool GetButtonRBDown ()
	{
		if (isMac)
			return Input.GetKeyDown (KeyCode.LeftApple);
		else
			return Input.GetKeyDown (KeyCode.LeftAlt);
	}
	public override bool GetButtonXDown ()
	{
		return Input.GetKeyDown (KeyCode.X);
	}
	public override bool GetButtonYDown ()
	{
		return Input.GetKeyDown (KeyCode.Z);
	}
	public override bool GetButtonYHold ()
	{
		return Input.GetKey (KeyCode.Z);
	}
	public override float GetLeftStickH ()
	{
		if (Input.GetKey (KeyCode.RightArrow) && !Input.GetKey (KeyCode.LeftArrow))
			return 1f;
		if (!Input.GetKey (KeyCode.RightArrow) && Input.GetKey (KeyCode.LeftArrow))
			return -1f;
		else
			return 0f;
	}
	public override bool GetLeftStickHold ()
	{
		return Input.GetKey (KeyCode.Question);
	}
	public override float GetLeftStickV ()
	{
		if (Input.GetKey (KeyCode.UpArrow) && !Input.GetKey (KeyCode.DownArrow))
			return 1f;
		if (!Input.GetKey (KeyCode.UpArrow) && Input.GetKey (KeyCode.DownArrow))
			return -1f;
		else
			return 0f;
	}
	public override bool GetLeftTriggerHold ()
	{
		return Input.GetKey (KeyCode.LeftShift);
	}
	public override float GetRightStickH ()
	{
		if (Input.GetKey (KeyCode.D) && !Input.GetKey (KeyCode.A))
			return 1f;
		if (!Input.GetKey (KeyCode.D) && Input.GetKey (KeyCode.A))
			return -1f;
		else
			return 0f;
	}
	public override bool GetRightStickPress ()
	{
		if (isMac)
			return Input.GetKeyDown (KeyCode.RightApple);
		else
			return Input.GetKeyDown (KeyCode.RightAlt);
	}
	public override float GetRightStickV ()
	{
		if (Input.GetKey (KeyCode.W) && !Input.GetKey (KeyCode.S))
			return 1f;
		if (!Input.GetKey (KeyCode.W) && Input.GetKey (KeyCode.S))
			return -1f;
		else
			return 0f;
	}
	public override bool GetRightTriggerHold ()
	{
		return Input.GetKey (KeyCode.Tab);
	}
	public override bool IsLeftStickDown ()
	{
		return Input.GetKey(KeyCode.DownArrow);
	}
	public override bool IsLeftStickLeft ()
	{
		return Input.GetKey (KeyCode.LeftArrow);
	}
	public override bool IsLeftStickRight ()
	{
		return Input.GetKey (KeyCode.RightArrow);
	}
	public override bool IsLeftStickUp ()
	{
		return Input.GetKey (KeyCode.UpArrow);
	}
}
