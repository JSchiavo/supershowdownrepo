﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;

public abstract class InputHandler {

	protected Dictionary<string, string> controlMappings;
	public abstract bool GetButtonADown ();
	public abstract bool GetButtonAHold ();
	public abstract bool GetButtonBDown ();
	public abstract bool GetButtonXDown ();
	public abstract bool GetButtonYDown ();
	public abstract bool GetButtonYHold ();
	public abstract bool GetButtonRBDown ();
	public abstract bool GetButtonLBDown ();
	public abstract bool GetLeftStickHold ();
	public abstract float GetLeftStickH ();
	public abstract float GetLeftStickV ();
	public abstract float GetRightStickH ();
	public abstract float GetRightStickV ();
	public abstract bool IsLeftStickLeft ();
	public abstract bool IsLeftStickRight ();
	public abstract bool IsLeftStickUp ();
	public abstract bool IsLeftStickDown ();
	public abstract bool GetRightStickPress ();
	public abstract bool GetLeftTriggerHold ();
	public abstract bool GetRightTriggerHold ();
}
