﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileLogic : MonoBehaviour {

	private bool hasSet;
	private Base target;
	private Rigidbody rigid;
	public float maxSpeed;
	public float lifeSpan;
	private float dieTimer;

	public GameObject onCollision;

	void Start() {
		rigid = gameObject.GetComponent<Rigidbody> ();
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (hasSet) {
			Transform trans = target.m.sM.isPinned ? target.m.pM.playerTransform : target.m.sM.ragdollRootTransform;
			transform.LookAt (trans.position + Vector3.up * 2f);
			//rigid.rotation = Quaternion.Lerp (rigid.rotation, Quaternion.LookRotation (target.m.pM.playerTransform.position + Vector3.up * 2f - transform.position), 0.15f);
			rigid.velocity = Vector3.Lerp (rigid.velocity, rigid.transform.forward * maxSpeed, 0.05f);
		}
		if (Time.time > dieTimer) {
			Destroy (gameObject);
		}
	}

	public void SetTarget(Base target) {
		this.target = target;
		hasSet = true;
		gameObject.GetComponent<Rigidbody>().velocity = transform.forward * maxSpeed * 1.5f;
		dieTimer = Time.time + lifeSpan;
	}

	void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.layer == 9) {
			Base b = collision.gameObject.GetComponentInParent<Base> ();
			if (b == target) {
				target.e.OnAttackReceived (target, new AttackData (FightingState.Weak, MuscleGroup.Arms, -collision.relativeVelocity.normalized, 10f, 1f), collision);
				GameObject.Instantiate (onCollision, collision.contacts [0].point, Quaternion.LookRotation (collision.contacts [0].normal));
				target.game.audioManager.PlayAudio ("ShotImpact", this, false);
				Destroy (gameObject);
			}
		} else {
			GameObject.Instantiate (onCollision, collision.contacts [0].point, Quaternion.LookRotation (collision.contacts [0].normal));
			target.game.audioManager.PlayAudio ("ShotImpact", this, true);
			Destroy (gameObject);
		}
	}
}
