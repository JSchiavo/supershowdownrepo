﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(MeshRenderer), typeof(MeshFilter))]
public class ProceduralBeam : MonoBehaviour {

	[Range(0.1f, 10f)]
	public float depthSegmentLength;
	public int segments;
	public float radius;
	[Range(0.1f, 60f)]
	public float speed;
	[Range(1f, 10f)]
	public float height;
	[Range(0.1f, 1f)]
	public float scale;
	public bool stop;

	List<Point> vertices;
	List<int> triangles;
	List<Vector3> noise;
	Mesh mesh;
	float depth;
	bool cap;

	// Use this for initialization
	void Start () {
		mesh = new Mesh ();
		mesh.MarkDynamic ();
		vertices = new List<Point> ();
		triangles = new List<int> ();
		noise = new List<Vector3> ();
		StartCoroutine (CreateBeam ());
	}

	IEnumerator CreateBeam() {
		depth = depthSegmentLength;
		float angleDelta = 360f / (float)segments;
		float currentRadius = radius / 10f;
		float expansionRate = 0.15f;

		vertices.Add (Point.zero);
		//Starting point
		float currentAngle = angleDelta;
		for (int w = 0; w < segments; w++) {
			Point point = new Point();
			point.radX = Mathf.Cos (currentAngle * Mathf.Deg2Rad);
			point.radY = Mathf.Sin (currentAngle * Mathf.Deg2Rad);
			point.x = point.radX * currentRadius;
			point.y = point.radY * currentRadius;
			point.z = depth;
			vertices.Add (point);
			currentAngle += angleDelta;
		}
		for (int i = 1; i < vertices.Count; i++) {
			triangles.Add (i);
			triangles.Add (0);
			triangles.Add ((i+1 > vertices.Count-1 ? 1 : i+1));
			AddNoise ();

		}
		yield return null;
		depth += depthSegmentLength;

		for (int i = 0; i < 60; i++) {
			if (cap)
				RemoveCap ();
			currentAngle = i % 2 == 0 ? angleDelta + (angleDelta/2f) : angleDelta;
			currentRadius = Mathf.Lerp (currentRadius, radius, expansionRate);
			for (int w = 0; w < segments; w++) {
				Point point = new Point ();
				point.radX = Mathf.Cos (currentAngle * Mathf.Deg2Rad);
				point.radY = Mathf.Sin (currentAngle * Mathf.Deg2Rad);
				point.x = point.radX * currentRadius;
				point.y = point.radY * currentRadius;
				point.z = depth;
				vertices.Add (point);
				currentAngle = currentAngle + angleDelta > 360f ? (currentAngle + angleDelta) - 360f : currentAngle + angleDelta; 
			}
			for (int w = 0; w < segments; w++) {
				triangles.Add ((vertices.Count - 1) - segments - w);
				triangles.Add (w == 0 ? ((vertices.Count - 1) - segments * 2 + 1): ((vertices.Count - 1) - segments) + 1 - w);
				triangles.Add (vertices.Count - 1 - w);

				triangles.Add ((vertices.Count - 1) - w);
				triangles.Add (w == segments - 1 ? vertices.Count - 1 : (vertices.Count - 1) - w - 1);
				triangles.Add ((vertices.Count - 1) - segments - w);
			}
			AddCap ();
			AddNoise ();
			yield return null;
			depth += depthSegmentLength;
		}

		while (!stop) {
			AddNoise ();
			yield return null;
		}
	}

	private void AddCap() {
		Point endPoint = Point.zero;
		endPoint.z = depth + depthSegmentLength * 3f;
		int endPos = vertices.Count - 1;
		vertices.Add (endPoint);
		for (int i = 0; i < segments; i++) {
			triangles.Add (vertices.Count - 1);
			triangles.Add (i == segments - 1 ? endPos : endPos - i - 1);
			triangles.Add (endPos - i);
		}
		cap = true;
	}

	private void RemoveCap() {
		vertices.RemoveAt (vertices.Count - 1);
		for (int i = 0; i < segments * 3; i++) {
			triangles.RemoveAt (triangles.Count - 1);
		}
		cap = false;
	}

	private void AddNoise() {
		noise.Clear ();
		for (int i = 0; i < vertices.Count; i++) {
			Point point = vertices [i];
			float offset = Mathf.PerlinNoise (((point.x - point.z) * scale) + (Time.time * speed), ((point.y - point.z) * scale) + (Time.time * speed)) * height;
			Vector3 noisePoint = new Vector3();
			noisePoint.x = point.x + (Mathf.Pow(point.x, 2) < 4f ? (offset * point.radX) / 10f: offset * point.radX);
			noisePoint.y = point.y + (Mathf.Pow(point.y, 2) < 4f ? (offset * point.radY) / 10f: offset * point.radY);
			noisePoint.z = point.z;

			noise.Add (noisePoint);
		}

		mesh.vertices = noise.ToArray ();
		mesh.triangles = triangles.ToArray ();

		mesh.RecalculateNormals ();
		//mesh.RecalculateBounds ();

		gameObject.GetComponent<MeshFilter> ().mesh = mesh;
	}

	private struct Point {
		public Vector3 point;
		public float radX;
		public float radY;

		public Point (Vector3 point, float radX, float radY) {
			this.point = point;
			this.radX = radX;
			this.radY = radY;
		}

		public static Point zero {
			get {
				return new Point (Vector3.zero, 0f, 0f);
			}
		}

		public float x {
			get {
				return point.x;
			}
			set {
				point.x = value;
			}
		}

		public float y {
			get {
				return point.y;
			}
			set {
				point.y = value;
			}
		}

		public float z {
			get {
				return point.z;
			}
			set {
				point.z = value;
			}
		}
	}
}
