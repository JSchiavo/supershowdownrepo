﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ProceduralTest : MonoBehaviour {

	private Mesh mesh;
	private List<Vector3> vertices;
	private MeshFilter meshFilter;
	private float depth = 0;

	// Use this for initialization
	void Start () {
		mesh = new Mesh ();
		meshFilter = gameObject.GetComponent<MeshFilter> ();
		meshFilter.mesh = mesh;
		vertices = new List<Vector3> ();
	}
	
	// Update is called once per frame
	void Update () {
		mesh.Clear ();

		vertices.Add(new Vector3 (0f, 0f, depth));
		vertices.Add(new Vector3 (1f, 0f, depth));
		vertices.Add(new Vector3 (1f, 1f, depth));

		mesh.vertices = vertices.ToArray ();

		int[] newTriangles = new int[vertices.Count];

		for (int x = 0; x < newTriangles.Length; x++) {
			newTriangles [x] = x;
		}
		mesh.triangles = newTriangles;
		depth++;
	}
}
