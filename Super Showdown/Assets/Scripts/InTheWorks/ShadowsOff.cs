﻿using UnityEngine;
using System.Collections;

public class ShadowsOff : MonoBehaviour {

	private MeshRenderer[] meshes;

	// Use this for initialization
	void Start () {
		meshes = gameObject.GetComponentsInChildren<MeshRenderer> ();
		foreach (MeshRenderer mesh in meshes) {
			mesh.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
		}
	}
}
